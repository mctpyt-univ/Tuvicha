===========================
  Tuvicha: guía de estilo
===========================


Acá se listan varios lineamientos de estilo que se usan actualmente en el
proyecto, tanto para el código fuente como para la documentación. Se intenta
que la lista sea exhaustiva, pero probablemente unos cuantos detalles hayan
pasado desapercibidos o no hayan recibido la atención que merecen.


Código fuente en general
========================

- Todo archivo de código fuente se ubica en el directorio ``fuente``.

- Cada línea contiene a lo sumo una instrucción.

- La longitud máxima de cada línea es 77 caracteres.

- Los nombres de las constantes van en mayúscula y usan guion bajo como
  separador. Lo mismo se aplica a las macros usadas como constantes.

- Los nombres de las variables van en minúscula y usan guion bajo como
  separador.

- Los nombres de las funciones van en minúscula y usan guion bajo como
  separador.

- Salvo algunos casos particulares, todos los textos están en español. Se
  consideran textos en un sentido amplio: los textos que forman parte de la
  interfaz de usuario, los símbolos y comentarios del código fuente, los
  nombres de archivos y directorios y demás.

Abreviaturas
------------

En las cadenas de la interfaz de usuario y en los comentarios no se admiten
abreviaturas. En los símbolos y los nombres de archivos y directorios, por
su parte, se admiten las siguientes abreviaturas y se las usa siempre que sea
posible, con la intención de hacer menos engorroso el desarrollo:

dir
    Directorio.

hexa
    Hexadecimal (base numérica 16).

id
    Identificador.

ma
    Multiarranque (en referencia a la Especificación de Multiarranque).


Código fuente de ensamblador
============================

Esto se aplica solamente a los archivos dedicados.

- Se emplea la sintaxis de Intel; más particularmente, la del ensamblador
  NASM.

- Las instrucciones se sangran con ocho espacios, contados a partir del
  principio de la línea.

- Los argumentos de las instrucciones se sangran al segundo nivel de
  tabulación, considerado de 8 caracteres de longitud, contado a partir del
  principio de la línea.


Código fuente en C
==================

- Se sigue la convención de Kernighan y Ritchie para el sangrado y la
  ubicación de las llaves:

    - Los contenidos de todo bloque se sangran con 4 espacios, contados a
      partir del nivel de sangrado superior.
    - Las llaves se sangran al nivel del bloque contenedor.
    - Si se trata de una definición de función o si el bloque no tiene
      encabezado, la llave de apertura va en una línea dedicada.
    - En caso contrario, la llave de apertura va en la misma línea que el
      encabezado del bloque, separada del mismo por un espacio.
    - Las llaves de cierre van en una línea dedicada.
    - En las llamadas a función, no hay espacio entre el nombre de la función
      y sus argumentos.
    - Las palabras reservadas ``if``, ``while`` y ``for`` van sucedidas por
      un espacio.
    - Si hay una llave de cierre inmediatamente antes de una palabra
      reservada ``else``, ambas van en la misma línea, separadas por un
      espacio.

- La coma como operador binario lleva un espacio a la derecha pero ninguno a
  la izquierda. El resto de operadores binarios lleva un espacio de cada lado
  (así se separa de sus operandos).

- Un módulo está determinado por un archivo de cabecera (con extensión
  ``.h``) y un archivo de implementación (con extensión ``.c``), ambos con el
  mismo nombre base (es decir, el nombre solo varía en la extensión). Tal
  nombre base se emplea como prefijo de todos los símbolos globales definidos
  por el módulo, con las siguientes excepciones:

    - el módulo ``utilidades/comun``;
    - los módulos del directorio ``fuente/comandos``;
    - cualquier módulo que indique lo contrario en el comentario inicial de
      su archivo de cabecera.

- Todo archivo de cabecera contiene, tras el comentario inicial (o al
  principio si no tiene tal comentario), las siguientes 2 líneas::

      #ifndef TUVICHA__<MODULO>__H
      #define TUVICHA__<MODULO>__H

  donde ``<MODULO>`` es la ruta del archivo fuente relativa al directorio
  ``fuente``, con componentes separados por un guion bajo (``_``) y sin la
  extensión del nombre (por ejemplo, para ``comandos/ayuda.h`` tendríamos
  ``COMANDOS_AYUDA``). Y al final de los archivos de cabecera va la línea::

      #endif

  Así se impide que el archivo pueda ser incluido más de una vez. 2 líneas en
  blanco deben suceder a las 2 líneas iniciales y preceder a la línea final.

- Todas las directivas ``#include`` van inmediatamente después del comentario
  inicial del archivo (o al principio si no tiene tal comentario, o tras las
  líneas indicadas en el punto anterior si es una cabecera). Si se trata de
  un archivo de implementación correspondiente a un módulo, la primera
  inclusión debe ser la cabecera del mismo módulo. El resto de las
  inclusiones se ordena primero según su directorio (en el orden: directorio
  actual, directorio raíz de fuentes, otros subdirectorios de fuentes) y
  después alfabéticamente.

- Los nombres de los tipos van capitalizados y no usan separador (es decir,
  se emplea la notación “CamelCase”).


Documentación
=============

Lo siguiente se aplica a los archivos de documentación redactados dentro del
proyecto. Se excluye el archivo de licencia, que fue tomado del sitio web de
GNU.

- Todo archivo de documentación se ubica en el directorio ``documentacion``.

- Se emplea el formato de seudomarcado *reStructuredText*, con la excepción
  de ''informe.txt'', que está en *LaTeX*.

- Se escribe en español.
