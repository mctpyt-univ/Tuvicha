==================
  Tuvicha: leeme
==================


**Tuvicha** es un sistema operativo sencillo. Ofrece al usuario una interfaz
de línea de comandos, la cual le permite acceder a información diversa: puede
inspeccionar la tabla de particiones de algún disco duro y hasta navegar por
sistemas de archivos FAT32.

Está hecho específicamente para PC con procesador de 32 o 64 bits. No va a
funcionar en otros tipos de computadoras.

El sistema es compatible con la Especificación de Multiarranque (“Multiboot
Specification”), lo que implica que puede ser cargado por cualquier gestor
de arranque que siga tal especificación (el caso insignia es GRUB).

Tuvicha es **software libre**: esto quiere decir que respeta tus derechos de
usarlo, adaptarlo y compartirlo libremente, con cualquier propósito. Está
publicado bajo la *Licencia Pública General de GNU* (GNU GPL), en su versión
3 o posterior. El texto completo de la misma se encuentra disponible, en
inglés, en el archivo ``documentacion/licencia.txt``.

Se incluye también un arrancador propio, que se encarga de cargar el núcleo
en memoria y empezar a ejecutarlo. Sin embargo el mismo está obsoleto, ya que
el núcleo se modificó para que cumpliera con la Especificación de
Multiarranque pero el arrancador no. La razón por la que no se borra es la
intención de usarlo de base algún día en el armado de un nuevo arrancador.


Requisitos
==========

Las siguientes aplicaciones son necesarias para compilar el sistema:

NASM
    Para ensamblar. En general otros ensambladores no van a funcionar porque
    cada uno tiene sus particularidades de sintaxis.

GCC
    Para compilar. O si no, un compilador para modo protegido de i386 que
    disponga de las extensiones para código ensamblador embebido y atributos
    de estructuras.

GNU LD
    Para enlazar. No puede ser otro ya que se emplea un script de LD.

El siguiente hardware es lo mínimo necesario para cargar y usar el sistema
(en la sección *Detalles técnicos* se ofrece más información):

- PC de 32 bits
- Muy poca RAM (a lo mejor con 1 MiB sobre, aunque no se hicieron mediciones)
- Monitor
- Teclado


Compilación
===========

Para compilar Tuvicha, ejecutá el siguiente comando::

    $ make

Se va a generar un archivo ``tuvicha.bin``, que contiene el sistema operativo
en su totalidad.

Si querés compilar en modo de depuración (útil para encontrar errores en el
programa), podés usar este otro comando::

    $ make DEPURAR=s

Para cargar el sistema, podés optar entre usar una máquina real o una máquina
virtual.

En el primer caso, es recomendable que lo cargues con el gestor de arranque
GRUB. Simplemente agregá en el menú de arranque una entrada con la ruta al
archivo ``tuvicha.bin`` generado antes y cuando reinicies, GRUB te va a
permitir cargar Tuvicha.

Si preferís una máquina virtual, podés usar QEMU, que es la que se usó para
desarrollar Tuvicha. Instalá QEMU en tu sistema y después ejecutá este
comando::

    $ make mv

El comando anterior te lanza una máquina virtual sin discos duros. Si querés
aprovechar los comandos de manejo de particiones y FAT32, tenés que generar
un disco duro virtual. Para esto, en caso de que uses GNU/Linux, Tuvicha te
ofrece dos comandos::

    $ make disco
    # make discop

Ambos crean un archivo ``disco.img`` con el contenido de un disco duro
virtual de 50 MiB. El primero lo formatea con un sistema de archivos FAT32
vacío. El segundo genera un MBR con tabla de particiones y una partición
primaria que ocupa el resto del disco, y formatea la partición con FAT32.
Notá que el segundo debe ejecutarse como superusuario ya que emplea un
dispositivo “loopback” de Linux; para poder después acceder como usuario
normal al disco creado, vas a tener que cambiar el propietario::

    # chown <usuario> disco.img

donde ``<usuario>`` es el nombre de tu usuario en el sistema.

Una vez disponible el disco virtual, este comando te lanza una máquina
virtual que lo usa::

    $ make mvd

Navegar un sistema de archivos vacío no tiene mucha gracia. Por eso es buena
idea que montes el FAT32 del disco virtual en tu sistema principal y le
agregues algunas cosas. En sistemas Unix (como GNU/Linux), tenés el comando
``mount`` para montar. Tené en cuenta que, si el disco virtual tiene
particiones, no podés montar directamente el archivo ya que es necesario
saltearse los primeros 512 bytes (que corresponden al MBR, donde está la
tabla de particiones); por lo tanto vas a tener que generar manualmente un
dispositivo de “loopback” y después montar este; en GNU/Linux lo podés hacer
con el comando ``losetup``.


Razón de ser
============

Tuvicha surgió como un proyecto académico: un trabajo final para la
asignatura *Arquitectura del Computador* en la carrera *Licenciatura de
Ciencias de la Computación*. Su propósito era aprender sobre el
funcionamiento de las PC con x86 en su nivel más crudo (sin ningún sistema
operativo facilitando las cosas). Sin duda tal propósito se cumplió con
creces.

La consigna inicial era hacer un programa en modo real (16 bits) que se
instalara en el MBR (de modo que fuera arrancado directamente por la BIOS) y
mostrara la tabla de particiones del mismo. Hacer eso ya enseña muchas
cosas. Pero para que fuera más entretenido, al autor se le ocurrió hacer que
el programa funcionara en modo protegido (32 bits) y que permitiera navegar
por algún sistema de archivos. Y eso acarreó muchísimo más aprendizaje, no
solo sobre esas cuestiones puntuales sino también sobre un montón de cosas
que fueron surgiendo en el camino y que transformaron radicalmente el
proyecto. Al programa original uno probablemente no lo llamaría sistema
operativo, pero lo que terminó generándose puede llegar a considerarse un
sistema operativo (bien primitivo, claro).

Ante el desconocimiento en materia de sistemas de archivos, desde temprano
se eligió FAT32 por la suposición de que sería uno de los más simples, si no
el más, y por su popularidad. En efecto, el mismo resultó muy fácil de
entender.

Antes de empezar a escribir Tuvicha, se hizo una prueba de concepto para ver
si se lograba implementar un navegador de FAT32 en espacio de usuario de
GNU/Linux (y con una interfaz de navegación en ncurses linda, de paso). El
resultado fue muy bueno y está disponible acá:

    http://gitlab.com/mstreet_fceia/dikvojo


Autor y desarrollo
==================

El autor de Tuvicha es Mariano Street. Podés contactarlo escribiéndole un
correo a ``mctpyt@gmail.com``.

El desarrollo del proyecto se organiza en torno al siguiente repositorio,
desde el cual se puede descargar el código fuente:

    http://gitlab.com/mstreet_fceia/tuvicha


Detalles técnicos
=================

Tuvicha podría considerarse un núcleo megalítico de sistema operativo
monousuario y monotarea. Es megalítico en el sentido de que no ejecuta
procesos y engloba toda la funcionalidad del sistema; debido a esto, en
en Tuvicha los conceptos de “núcleo” y de “sistema operativo” son lo mismo.

El código fuente está escrito mayormente en el lenguaje de programación C,
más específicamente en su versión estándar de 1999 (C99), con algunas
extensiones de GCC. También contiene, allá donde se torne necesario o
conveniente, pedazos en ensamblador para i386, en dos formas posibles:

1. Embebido en el código en C, con sintaxis de AT&T (ensamblado por GNU AS).
2. En archivos dedicados, con sintaxis de Intel (ensamblado por NASM).

Formatos de hardware
--------------------

Se emplean las siguientes tecnologías de hardware, disponibles en cualquier
PC con procesador i386 o superior (a veces estas tecnologías fueron sucedidas
por otras, pero se mantienen emuladas por retrocompatibilidad):

CPU
    i386 en modo protegido de 32 bits, con segmentación plana, sin
    paginación.

Interrupciones
    PIC 8259.

Teclado
    AT.

Video
    CGA en modo texto de 80 columnas por 25 filas.

Discos duros
    ATA-2.

Formatos de software
--------------------

En cuanto al software, se emplean los siguientes formatos:

Mapas de teclado
    Dvorak y QWERTY (en inglés de Estados Unidos).

Particiones
    MBR (solo se reconocen particiones primarias).

Sistema de archivos
    FAT32, sin la extensión para nombres largos (LFN) de VFAT.
