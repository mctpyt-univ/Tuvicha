/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Módulo para instalar y ejecutar comandos.                               *
 ***************************************************************************/
#ifndef TUVICHA__LINEACOMANDO__H
#define TUVICHA__LINEACOMANDO__H


#include "utilidades/comun.h"


typedef void (*ComandoFuncion)(void);

typedef struct {
    const char    *nombre;
    const char    *descripcion;
    const char    *args;
    ComandoFuncion funcion;
} Comando;

#define LINEACOMANDO_INSTALAR_COMANDO(nombre)                     \
    lineacomando_instalar_comando(#nombre,                        \
                                  comando_##nombre##_descripcion, \
                                  comando_##nombre##_args,        \
                                  &comando_##nombre)

/**
 * Obtener el próximo argumento ingresado en la línea de comando.
 */
char          *lineacomando_arg(void);

/**
 * Obtener la cantidad de comandos instalados.
 */
uint8          lineacomando_cantidad_comandos(void);

/**
 * Obtener la base de datos de comandos, el vector donde se almacena la
 * información de los comandos instalados.
 */
const Comando *lineacomando_comandos(void);

/**
 * Ejecutar una línea de comando. Las líneas se consideran secuencias de
 * componentes separados por espacios; el primer componente se toma como
 * nombre del comando a ejecutar y los demás como sus argumentos.
 *
 * @param linea     cadena con la línea de comando a ejecutar.
 * @param longitud  número mayor o igual que la longitud de `linea'
 *                  (incluyendo el caracter nulo).
 */
void           lineacomando_ejecutar(const char        *linea,
                                     const unsigned int longitud);

void           lineacomando_establecer_comando_ayuda(const char *nombre);

/**
 * Instalar un comando, en caso de haber espacio en la base de datos de
 * comandos.
 *
 * @param nombre       cadena con el nombre del comando.
 * @param descripcion  cadena con una descripción de la función del comando.
 * @param args         cadena con una descripción de los argumentos que
 *                     admite el comando.
 * @param funcion      puntero a la función del comando.
 * @return verdadero si se pudo instalar el comando, falso si no (por falta de
 *         espacio).
 */
bool           lineacomando_instalar_comando(const char    *nombre,
                                             const char    *descripcion,
                                             const char    *args,
                                             ComandoFuncion funcion);

/**
 * Obtener el nombre del comando de ayuda.
 */
const char    *lineacomando_nombre_comando_ayuda(void);


#endif
