/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Advertencia: dado que este código está hecho actualmente para funcionar *
 * solo en x86, no hace falta reordenar bytes ya que tanto esta            *
 * arquitectura como los sistemas de archivos FAT emplean el orden         *
 * “little-endian”. Si se llegare a migrar esto a arquitecturas que        *
 * empleen “big-endian”, habrá que acomodar los bytes de aquellos enteros  *
 * que ocupen más de un byte.                                              *
 ***************************************************************************/


#include "fat32.h"
#include "disco.h"
#include "utilidades/asegurar.h"
#include "utilidades/memoria.h"


/**
 * Parece seguro asumir que, al menos por defecto, cada sector ocupa 512
 * bytes. En caso de permitirse otros tamaños de sector, habrá que procurar
 * que el número sea múltiplo de 32 para evitar problemas al leer directorios
 * y al leer la tabla FAT (o adaptar los respectivos códigos).
 */
#define BYTES_POR_SECTOR      512

/**
 * Longitud (cantidad de bytes) de la base de un nombre corto de archivo
 * (sin la extensión).
 */
#define LONGITUD_NOMBRE       8

/**
 * Longitud (cantidad de bytes) de una extensión de un nombre corto de
 * archivo.
 */
#define LONGITUD_EXTENSION    3

#define FIRMA_IDVOLUMEN       0xAA55
#define FIRMA_FSINFO_1        0x41615252  /* "RRaA" */
#define FIRMA_FSINFO_2        0x61417272  /* "rrAa" */
#define FIRMA_FSINFO_3        0xAA550000

/**
 * Estructura correspondiente al primer sector reservado de un sistema de
 * archivos FAT32.
 */
typedef struct {

    /* BPB (“BIOS Parameter Block”) */

    /** Instrucciones de salto para evitar que se ejecuten datos del “Boot
        Record” que no son código ejecutable. */
    uint8  instrucciones_salto[3];
    /** Cadena de identificación del OEM. */
    uint8  id_oem[8];
    /** Cantidad de bytes por sector. */
    uint16 bytes_por_sector;
    /** Cantidad de sectores por clúster. */
    uint8  sectores_por_cluster;
    /** Cantidad de sectores reservados, tras los cuales empiezan las FAT. */
    uint16 sectores_reservados;
    /** Cantidad de tablas FAT. */
    uint8  cantidad_fat;
    /** Cantidad de entradas en el directorio raíz. Siempre 0 en FAT32. */
    uint16 entradas_en_dir_raiz;
    /** Varios datos que no usamos. */
    uint8  ignorado0[17];

    /* EBR (“Extended Boot Record”) */

    /** Cantidad de sectores que ocupa cada tabla FAT. */
    uint32 sectores_por_fat;
    /** Varios datos que no usamos. */
    uint8  ignorado1[4];
    /** Número del primer clúster del directorio raíz. */
    uint32 primer_cluster_dir_raiz;
    /** Número del sector reservado donde se encuentra el “FSInfo”. */
    uint16 sector_fsinfo;
    /** Varios datos que no usamos. */
    uint8  ignorado2[BYTES_POR_SECTOR - 52];
    /** Firma de autenticidad del sistema de archivos. */
    uint16 firma;

} __attribute__((packed)) IDVolumen;

/**
 * Estructura correspondiente a algún sector reservado de un sistema de
 * archivos FAT32, posterior a `IDVolumen'.
 */
typedef struct {
    uint32 firma0;
    uint8  ignorado0[480];
    uint32 firma1;
    uint32 clusteres_libres;
    uint32 ultimo_cluster_asignado;
    uint8  ignorado1[12];
    uint32 firma2;
} __attribute__((packed)) FSInfo;

/** Representación de horas de FAT32. */
typedef struct {
    /** Período de 2 segundos: entre 0 y 29. Se debe multiplicar por 2 para
        convertir a segundos. */
    unsigned int segundo : 5;
    /** Minuto: entre 0 y 59. */
    unsigned int minuto  : 6;
    /** Hora: entre 0 y 23. */
    unsigned int hora    : 5;
} __attribute__((packed)) Hora;

/** Representación de fechas de FAT32. */
typedef struct {
    /** Día: entre 1 y 31. */
    unsigned int dia  : 5;
    /** Mes: entre 1 y 12. */
    unsigned int mes  : 4;
    /** Año, relativo a 1980 (por tanto el rango abarca desde 1980 hasta
        2099). */
    unsigned int anio : 7;
} __attribute__((packed)) Fecha;

typedef struct {
    char   nombre[LONGITUD_NOMBRE + LONGITUD_EXTENSION];
    uint8  atributos;
    uint8  ignorado0[8];
    uint16 cluster_alto;
    Hora   hora;
    Fecha  fecha;
    uint16 cluster_bajo;
    uint32 tamanio;
} __attribute__((packed)) EntradaDir;

/**
 * Estructura que representa un sistema de archivos FAT32 cargado. Contiene
 * información preprocesada que es necesaria para navegarlo.
 */
typedef struct {
    /** ¿Hay un sistema de archivos FAT32 cargado? */
    bool   cargado;
    /** Cantidad de sectores por clúster. */
    uint8  sectores_por_cluster;
    /** Dirección LBA absoluta del primer sector de la primera tabla FAT. */
    uint32 lba_inicio_tabla;
    /** Dirección LBA absoluta del primer sector del primer clúster. La
        región del sistema de archivos FAT32 donde se localizan los clústeres
        es donde se guardan los contenidos de archivos y directorios, lo que
        acá se llama datos. */
    uint32 lba_inicio_datos;
    /** Número del primer clúster del directorio raíz. */
    uint32 primer_cluster_dir_raiz;
} FAT32;

/**
 * Sector de una tabla FAT cargado en RAM.
 */
typedef struct {
    uint32 sector[BYTES_POR_SECTOR / sizeof (uint32)];
    /** ¿Hay un sector de la tabla FAT cargado en RAM? En otras palabras,
        ¿sirven los datos de `sector'? */
    bool   cargada;
    /** Número de sector de la tabla FAT cargado. El primer sector de la
        tabla FAT se corresponde con la posición 0, el segundo con la
        posición 1 y así sucesivamente. */
    uint8  posicion;
} Tabla;

/**
 * Sector de un directorio cargado en RAM.
 */
typedef struct {
    EntradaDir sector[BYTES_POR_SECTOR / sizeof (EntradaDir)];
    /** ¿Hay un sector de algún directorio cargado en RAM? En otras palabras,
        ¿sirven los datos de `sector'? */
    bool       cargado;
    /** Posición actual en el recorrido del sector cargado. Índice para
        `sector'. */
    uint8      posicion;
    /** Número de sector del clúster cargado. El primer sector del clúster se
        corresponde con la posición 0, el segundo con la posición 1 y así
        sucesivamente. */
    uint8      posicion_en_cluster;
    /** Clúster al que corresponde el sector cargado. */
    uint32     cluster;
    /** Dirección LBA absoluta del clúster indicado por `cluster'. */
    uint32     lba_cluster;
} Dir;


static FAT32 fat32;

/**
 * Se trabaja con un único sector de la tabla FAT a la vez, el cual se
 * mantiene siempre cargado en RAM.
 */
static Tabla tabla;

/**
 * Se trabaja con un único sector del contenido del directorio a la vez, el
 * cual se mantiene siempre cargado en RAM.
 */
static Dir   dir;


static inline bool es_sistema_archivos_fat32(const IDVolumen *volumen)
{
    if (volumen->firma != FIRMA_IDVOLUMEN)
        return false;
    if (volumen->bytes_por_sector != BYTES_POR_SECTOR)
        /* Esto en realidad no implica que no sea FAT32 el sistema de
           archivos, pero no sabemos manejar otros tamaños de sector. */
        return false;
    if (volumen->entradas_en_dir_raiz != 0)
        return false;
    return true;
}

static inline void cargar_info(FAT32Info   *info,
                               IDVolumen   *volumen,
                               const uint32 lba_inicial)
{
    info->sectores_por_cluster    = volumen->sectores_por_cluster;
    info->sectores_reservados     = volumen->sectores_reservados;
    info->cantidad_fat            = volumen->cantidad_fat;
    info->sectores_por_fat        = volumen->sectores_por_fat;
    info->primer_cluster_dir_raiz = volumen->primer_cluster_dir_raiz;
    memoria_copiar(info->id_oem, volumen->id_oem, sizeof volumen->id_oem);
    info->id_oem[sizeof info->id_oem - 1] = '\0';

    /* Obtenemos la cantidad de clústeres libres, en caso de estar
       disponible. */
    if (volumen->sector_fsinfo == (uint16) -1 ||
        volumen->sector_fsinfo == 0 ||
        volumen->sector_fsinfo >= volumen->sectores_reservados)
        info->clusteres_libres = (uint32) -1;
    else {
        FSInfo fsinfo;

        if (disco_leer(lba_inicial + volumen->sector_fsinfo,
                       1, (uint16 *) &fsinfo) &&
            fsinfo.firma0 == FIRMA_FSINFO_1 &&
            fsinfo.firma1 == FIRMA_FSINFO_2 &&
            fsinfo.firma2 == FIRMA_FSINFO_3)
            info->clusteres_libres = fsinfo.clusteres_libres;
        else
            info->clusteres_libres = (uint32) -1;
    }
}

/**
 * Obtener el número relativo de sector en la tabla FAT en el cual se
 * encuentra la entrada del clúster dado.
 */
static inline uint32 sector_de_cluster(const uint32 cluster)
{
    /** Considerando 512 bytes por sector y 4 bytes por número de clúster, se
        tiene que los 25 bits más significativos de tal número indican el
        sector a examinar. */
    return cluster / (BYTES_POR_SECTOR / sizeof cluster);
}

/**
 * Obtener el índice correspondiente a la entrada del clúster dado en el
 * sector de la tabla FAT que la contiene.
 */
static inline uint32 indice_de_cluster(const uint32 cluster)
{
    /* Considerando 512 bytes por sector y 4 bytes por número de clúster, se
       tiene que los 7 bits menos significativos de tal número indican el
       índice dentro del sector (ya que en cada sector entran exactamente 128
       números de clúster). */
    return cluster & (BYTES_POR_SECTOR / sizeof cluster - 1);
}

/**
 * Obtener el número del clúster siguiente al dado en la cadena a la que este
 * pertenece dentro de la tabla FAT.
 */
static uint32 cluster_siguiente(uint32 cluster)
{
    if (!tabla.cargada || sector_de_cluster(cluster) != tabla.posicion) {
        /* Cargamos el sector de la tabla FAT correspondiente al clúster
           indicado. */
        tabla.posicion = sector_de_cluster(cluster);
        if (!(tabla.cargada = disco_leer(fat32.lba_inicio_tabla +
                                         tabla.posicion, 1,
                                         (uint16 *) tabla.sector)))
            return 0;
    }

    /* Obtenemos el valor almacenado en la FAT, pero solo tenemos en cuenta
       los 28 bits menos significativos (los otros 4 están reservados). */
    cluster = tabla.sector[indice_de_cluster(cluster)] & MANTENER_BITS(28);

    /* Si el valor obtenido es...
       ... 0,                    entonces el clúster está vacío.
       ... 1,                    entonces el clúster está reservado.
       ... 0xFFFFFF7,            entonces el clúster está corrupto.
       ... 0xFFFFFF8 o superior, entonces no hay más clústeres en la cadena.
       En todos estos casos devolvemos 0, que indica que hay que dejar de
       recorrer la cadena.
    */
    if (cluster == 1 || cluster >= 0xFFFFFF7)
        cluster = 0;
    return cluster;
}

static uint32 lba_cluster(uint32 cluster)
{
    asegurar(cluster >= 2);

    return fat32.lba_inicio_datos +
           (cluster - 2) * fat32.sectores_por_cluster;
}

static inline void copiar_nombre_enlindecido(char       *destino,
                                             const char *origen)
{
    unsigned int i, espacios_finales;
    char        *extension;

    asegurar(destino != NULL);
    asegurar(origen != NULL);

    /* Copiamos la base del nombre. */
    for (i = espacios_finales = 0; i < LONGITUD_NOMBRE; ++i)
        if ((destino[i] = origen[i]) == ' ')
            ++espacios_finales;
        else
            espacios_finales = 0;

    /* Copiamos la extensión, sobrescribiendo espacios finales en la
       base del nombre. */
    extension = destino + LONGITUD_NOMBRE + 1 - espacios_finales;
    for (i = espacios_finales = 0; i < LONGITUD_EXTENSION; ++i)
        if ((extension[i] = origen[i + LONGITUD_NOMBRE]) == ' ')
            ++espacios_finales;
        else
            espacios_finales = 0;

    if (espacios_finales == LONGITUD_EXTENSION)
        extension[-1] = '\0';
    else {
        extension[i - espacios_finales] = '\0';
        extension[-1] = '.';
    }
}

bool fat32_cargar(const uint32 lba_inicial,
                  FAT32Info   *info)
{
    IDVolumen volumen;

    /* Cargamos el “Boot Record” del sistema de archivos, que se corresponde
       con el primer sector del mismo. */
    if (!disco_leer(lba_inicial, 1, (uint16 *) &volumen))
        return false;

    /* Nos aseguramos de que lo que cargamos sea un sistema de archivos
       FAT32. */
    if (!(fat32.cargado = es_sistema_archivos_fat32(&volumen)))
        return false;

    fat32.sectores_por_cluster    = volumen.sectores_por_cluster;
    fat32.lba_inicio_tabla        = lba_inicial +
                                    volumen.sectores_reservados;
    fat32.lba_inicio_datos        = fat32.lba_inicio_tabla +
                                    volumen.cantidad_fat *
                                    volumen.sectores_por_fat;
    fat32.primer_cluster_dir_raiz = volumen.primer_cluster_dir_raiz;

    /* Si el llamante lo pide, le pasamos información. */
    if (info)
        cargar_info(info, &volumen, lba_inicial);
    return true;
}

bool fat32_navegar(FAT32Archivo *dir_a_navegar,
                   FAT32Archivo *entrada)
{
    asegurar(fat32.cargado);

    if (!dir.cargado ||
        dir_a_navegar && dir.cluster != dir_a_navegar->cluster) {

        if (dir_a_navegar) {
            /* Nos aseguramos de que la entrada sea de directorio. */
            if (!dir_a_navegar->atributos.a.directorio)
                return false;
            dir.cluster = dir_a_navegar->cluster;
        } else
            dir.cluster = fat32.primer_cluster_dir_raiz;

        dir.lba_cluster = lba_cluster(dir.cluster);
        /* Leemos el primer sector del clúster. */
        if (disco_leer(dir.lba_cluster, 1, (uint16 *) dir.sector)) {
            dir.cargado  = true;
            dir.posicion = 0;
        } else
            return false;
    }

    for (;; ++dir.posicion) {
        /* Si se alcanzó el fin del sector, cargamos el que sigue. */
        if (dir.posicion == LONGITUD(dir.sector)) {
            if (++dir.posicion_en_cluster < fat32.sectores_por_cluster) {
                /* Cargamos el siguiente sector del clúster actual. */
                if (!disco_leer(dir.lba_cluster + dir.posicion_en_cluster,
                                1, (uint16 *) dir.sector)) {
                    dir.cargado = false;
                    return false;
                }
            } else {
                /* Cargamos el primer sector del siguiente clúster. */
                if (!(dir.cluster = cluster_siguiente(dir.cluster))) {
                    dir.cargado = false;
                    return false;
                }
                dir.lba_cluster = lba_cluster(dir.cluster);
                if (!disco_leer(dir.lba_cluster, 1, (uint16 *) dir.sector)) {
                    dir.cargado = false;
                    return false;
                }
                dir.posicion_en_cluster = 0;
            }
            dir.posicion = 0;
        }
        /* Si se alcanzó el indicador de fin de directorio, terminamos. */
        if (dir.sector[dir.posicion].nombre[0] == 0) {
            dir.cargado = false;
            return false;
        }
        /* Ignoramos las entradas borradas. */
        if (((uint8) dir.sector[dir.posicion].nombre[0]) == 0xE5)
            continue;
        /* Ignoramos las entradas de nombres largos de VFAT. */
        if (dir.sector[dir.posicion].atributos == 0xF)
            continue;
        /* Si llegamos hasta acá es porque estamos ante una entrada normal,
           así que podemos salir del bucle. */
        break;
    }

    /* Le pasamos la información de la entrada al llamante. */
    copiar_nombre_enlindecido(entrada->nombre,
                              dir.sector[dir.posicion].nombre);
    entrada->atributos.n  = dir.sector[dir.posicion].atributos;
    entrada->hora.hora    = dir.sector[dir.posicion].hora.hora;
    entrada->hora.minuto  = dir.sector[dir.posicion].hora.minuto;
    entrada->hora.segundo = dir.sector[dir.posicion].hora.segundo * 2;
    entrada->fecha.anio   = dir.sector[dir.posicion].fecha.anio + 1980;
    entrada->fecha.mes    = dir.sector[dir.posicion].fecha.mes;
    entrada->fecha.dia    = dir.sector[dir.posicion].fecha.dia;
    entrada->tamanio      = dir.sector[dir.posicion].tamanio;
    entrada->cluster      = dir.sector[dir.posicion].cluster_bajo |
                            ((uint32) dir.sector[dir.posicion].cluster_alto)
                                      << 16;
    ++dir.posicion;
    return true;
}
