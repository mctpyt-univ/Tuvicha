/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Controlador de sistemas de archivos FAT32. Solo provee capacidad de     *
 * lectura, no escritura. No implementa la extensión de nombres largos     *
 * (LFN), que es parte de VFAT.                                            *
 ***************************************************************************/
#ifndef TUVICHA__FAT32__H
#define TUVICHA__FAT32__H


#include "utilidades/comun.h"
#include "utilidades/tiempo.h"


typedef struct {
    /** Cadena de identificación del OEM, terminada en nulo. */
    char   id_oem[9];
    uint8  sectores_por_cluster;
    uint16 sectores_reservados;
    uint8  cantidad_fat;
    uint32 sectores_por_fat;
    uint32 primer_cluster_dir_raiz;
    /** Cantidad de clústeres libres, o -1 si no se pudo determinar. */
    uint32 clusteres_libres;
} FAT32Info;

typedef struct {
    unsigned int sololectura : 1;
    unsigned int oculto      : 1;
    unsigned int sistema     : 1;
    unsigned int idvolumen   : 1;
    unsigned int directorio  : 1;
    unsigned int archivar    : 1;
    unsigned int ignorado    : 2;
} __attribute__((packed)) FAT32Atributos;

typedef struct {
    /** Nombre enlindecido. */
    char        nombre[13];
    /** Atributos. */
    union {
        uint8          n;
        FAT32Atributos a;
    }          atributos;
    /** Hora de modificación. */
    TiempoHora  hora;
    /** Fecha de modificación. */
    TiempoFecha fecha;
    /** Tamaño en sectores. */
    uint32      tamanio;
    /** Número del primer clúster. */
    uint32      cluster;
} FAT32Archivo;

/**
 * Leer un sistema de archivos FAT32 del disco actual, ubicado a partir de la
 * dirección LBA dada.
 */
bool         fat32_cargar(const uint32 lba_inicial,
                          FAT32Info   *info);

/**
 * Navegar un directorio del sistema de archivos FAT32 cargado.
 *
 * @param dir_a_navegar  entrada del directorio a navegar; si es NULL, se
 *                       emplea el directorio raíz.
 * @param entrada        entrada donde guardar los datos obtenidos.
 */
bool         fat32_navegar(FAT32Archivo *dir_a_navegar,
                           FAT32Archivo *entrada);


#endif
