/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "teclado.h"
#include "lector.h"
#include "puerto.h"
#include "utilidades/asegurar.h"
#include "utilidades/cadena.h"


/**
 * Puerto del teclado XT.
 */
#define TECLADO_PUERTO     0x60

#define RESPUESTA_ACK      0xFA

#define TECLA_RETROCESO    0x0E
#define TECLA_TABULACION   0x0F
#define TECLA_RETORNO      0x1C
#define TECLA_MAYUS_IZQ    0x2A
#define TECLA_MAYUS_DER    0x36
#define TECLA_F1           0x3B
#define TECLA_F2           0x3C
#define TECLA_F3           0x3D

#define TECLA_SOLTADA      0x80

/**
 * @param fila_numerica  cadena con los caracteres a imprimir para cada una
 *                       de las 12 teclas de la fila numérica (excluyendo
 *                       la primera tecla, que va en `fila_guia').
 * @param fila_superior  cadena con los caracteres a imprimir para cada una
 *                       de las 12 teclas de la fila superior.
 * @param fila_guia      cadena con los caracteres a imprimir para cada una
 *                       de las 12 teclas de la fila guía (la última tecla
 *                       es la primera de la fila numérica).
 * @param fila_inferior  cadena con los caracteres a imprimir para la última
 *                       tecla de la fila numérica o guía (dependiendo de
 *                       cada teclado) y cada una de las 10 teclas de la
 *                       fila inferior.
 */
#define TECLAS(fila_numerica,fila_superior,fila_guia,fila_inferior) \
    fila_numerica "\0\0" fila_superior "\0\0" fila_guia "\0" fila_inferior

/**
 * Longitud de las cadenas generadas por `TECLAS', asumiendo que se le pasen
 * argumentos de la longitud indicada.
 */
#define LONGITUD_TECLAS    0x35

#define INICIO_TECLAS      0x2

/**
 * Mapa de teclas imprimibles a caracteres ASCII.
 */
typedef struct {
    /** Cadena con el nombre del mapa de teclado. */
    char nombre[8];
    /** Cadenas con la disposición de teclas del mapa de teclado. La primera
        cadena corresponde al mapa de teclas normales (sin Mayús presionado)
        y la segunda, al mapa con Mayús presionado. */
    char teclas[2][LONGITUD_TECLAS];
} Mapa;


/**
 * Mapa que indica cuáles teclas se corresponden a caracteres imprimibles (de
 * las cuales los mapas seleccionables son un subconjunto propio) y cuáles
 * no. Está diseñado en torno al primer conjunto de códigos de escaneo (IBM
 * PC XT).
 *
 * \0 indica que una tecla especial, no imprimible.
 * \a indica una tecla imprimible cubierta por los mapas seleccionables.
 * Cualquier otro caracter indica una tecla imprimible no cubierta por los
 * mapas seleccionables, la cual se mapea a tal caracter.
 */
static const char imprimibles[] =
    "\0\0"                      /* ¿?, Escape                     */
    "\a\a\a\a\a\a\a\a\a\a\a\a"  /* Fila numérica                  */
    "\0\0"                      /* Retroceso, Tabulación          */
    "\a\a\a\a\a\a\a\a\a\a\a\a"  /* Fila superior                  */
    "\0\0"                      /* Retorno, Control               */
    "\a\a\a\a\a\a\a\a\a\a\a\a"  /* Fila guía                      */
    "\0"                        /* Mayús izquierdo                */
    "\a\a\a\a\a\a\a\a\a\a\a"    /* Fila inferior                  */
    "\0*\0 \0"                  /* Mayús derecho, Alt, Bloq Mayús */
    "\0\0\0\0\0\0\0\0\0\0"      /* F1..F10                        */
    "\0\0"                      /* Bloq Num, ¿?                   */
    "789-456+1230."             /* Teclado numérico               */
    "\0\0<\0\0";                /* ¿?, ¿?, F11, F12               */

/**
 * Mapas de teclado seleccionables. Representan un subconjunto propio de las
 * teclas imprimibles.
 */
static const Mapa mapas[] = {
    { "dvorak", { TECLAS("1234567890[]",  "',.pyfgcrl/=",
                         "aoeuidhtns-`", "\\;qjkxbmwvz"),
                  TECLAS("!@#$%^&*(){}", "\"<>PYFGCRL?+",
                         "AOEUIDHTNS_~",  "|:QJKXBMWVZ") }
    },
    { "qwerty", { TECLAS("1234567890-=", "qwertyuiop[]",
                         "asdfghjkl;'`", "\\zxcvbnm,./"),
                  TECLAS("!@#$%^&*()_+", "QWERTYUIOP{}",
                         "ASDFGHJKL:\"~", "|ZXCVBNM<>?") }
    }
};

/**
 * Cantidad de teclas Mayús presionadas: 0 (ninguna), 1 (izquierda o derecha)
 * o 2 (izquierda y derecha).
 */
static uint8      mayus;

/**
 * Índice del mapa de teclado seleccionado actualmente.
 */
static uint8      seleccion;


static inline void esperar_para_escribir(void)
{
    while (puerto_leer(TECLADO_CONTROLADOR_PUERTO) & 2);
}

static inline void esperar_para_leer(void)
{
    while (!(puerto_leer(TECLADO_CONTROLADOR_PUERTO) & 1));
}

bool teclado_cambiar_luces(const TecladoLuz luces)
{
    esperar_para_escribir();
    puerto_escribir(TECLADO_PUERTO, 0xED);
    esperar_para_leer();
    if (puerto_leer(TECLADO_PUERTO) != RESPUESTA_ACK)
        return false;

    esperar_para_escribir();
    puerto_escribir(TECLADO_PUERTO, luces);
    esperar_para_leer();
    if (puerto_leer(TECLADO_PUERTO) != RESPUESTA_ACK)
        return false;

    return true;
}

bool teclado_cambiar_mapa_actual(const char *nombre)
{
    unsigned int i;

    asegurar(nombre != NULL);

    for (i = 0; i < LONGITUD(mapas); ++i)
        if (cadena_iguales(nombre, mapas[i].nombre)) {
            seleccion = i;
            return true;
        }
    return false;
}

unsigned int teclado_cantidad_mapas(void)
{
    return LONGITUD(mapas);
}

void teclado_interpretar_tecla(void)
{
    /* Obtenemos la tecla presionada. */
    const uint8 tecla = puerto_leer(TECLADO_PUERTO);

    /* Se soltó una tecla. */
    if (tecla & TECLA_SOLTADA) {
        /* Solo nos importa si esa tecla es un Mayús. */
        switch (tecla) {
            case TECLA_MAYUS_IZQ | TECLA_SOLTADA:
            case TECLA_MAYUS_DER | TECLA_SOLTADA:
                mayus--;
                break;
        }
        return;
    }

    /* Se presionó una tecla. */
    switch (imprimibles[tecla]) {
        case '\0':  /* Tecla especial. */
            switch (tecla) {
                case TECLA_RETROCESO:
                    lector_borrar_cr();
                    break;
                case TECLA_TABULACION:
                    lector_autocompletar();
                    break;
                case TECLA_RETORNO:
                    lector_aceptar();
                    break;
                case TECLA_MAYUS_IZQ:
                case TECLA_MAYUS_DER:
                    mayus++;
                    break;
                case TECLA_F1:
                    lector_escribir_ayuda();
                    break;
                case TECLA_F2:
                    lector_moverse_en_historial(false);
                    break;
                case TECLA_F3:
                    lector_moverse_en_historial(true);
                    break;
            }
            break;
        case '\a':  /* Tecla imprimible de las manejadas por los mapas. */
            lector_escribir_cr(
                mapas[seleccion].teclas[!!mayus][tecla - INICIO_TECLAS]);
            break;
        default:  /* Tecla imprimible pero no manejada por los mapas. */
            lector_escribir_cr(imprimibles[tecla]);
    }
}

const char *teclado_mapa_actual(void)
{
    return mapas[seleccion].nombre;
}

const char *teclado_mapa(unsigned int indice)
{
    asegurar(indice < LONGITUD(mapas));

    return mapas[indice].nombre;
}
