/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Utilidades para manejar cadenas de caracteres ASCII terminadas en nulo. *
 ***************************************************************************/
#ifndef TUVICHA__UTILIDADES_CADENA__H
#define TUVICHA__UTILIDADES_CADENA__H


#include "comun.h"


/**
 * Espacio a reservar para las cadenas en que opere `cadena_desde_numero'.
 * Se define como la mayor cantidad posible de dígitos (que es igual a la
 * cantidad de bits del número a representar, ya que la representación más
 * extensa es la binaria), más el caracter nulo.
 */
#define CADENA_DESDE_NUMERO_LONGITUD         (sizeof (int) * 8 + 1)

/**
 * Espacio a reservar para las cadenas en que opere `cadena_desde_numero_
 * hexa'. Se define como la mayor cantidad posible de dígitos (que es 2
 * veces la cantidad de bytes del número a representar, porque 2 cifras
 * hexadecimales se corresponden con exactamente un byte), más el caracter
 * nulo.
 */
#define CADENA_DESDE_NUMERO_HEXA_LONGITUD    (sizeof (int) * 2 + 1)


/**
 * Convertir todas las letras minúsculas de una cadena en mayúsculas.
 *
 * @param cadena  cadena en la cual realizar la conversión.
 */
void         cadena_a_mayuscula(char *cadena);

/**
 * Cambiar el caracter de relleno.
 *
 * @sa cadena_relleno
 */
void         cadena_cambiar_relleno(const char relleno_nuevo);

/**
 * Obtener el próximo componente de una cadena. La cadena se trata como una
 * secuencia de componentes separados por un caracter dado. Los componentes
 * vacíos se ignoran.
 *
 * Cuidado: esta función no es reentrante.
 * Ciudado: esta función modifica la cadena original.
 *
 * @param cadena_dada  cadena de componentes a desmembrar, solo para obtener
 *                     el primer componente; para los que siguen, este
 *                     argumento debe ser NULL.
 * @param separador    caracter con el cual se separan los componentes.
 * @return puntero al inicio del próximo componente.
 */
char        *cadena_componente(char      *cadena_dada,
                               const char separador);

/**
 * Copiar el contenido de una cadena en otra.
 *
 * @return cantidad de caracteres copiados (excluyendo el caracter nulo).
 */
unsigned int cadena_copiar(char       *destino,
                           const char *origen);

/**
 * Comprobar si dos cadenas son iguales.
 */
bool         cadena_iguales(const char *cadena1,
                            const char *cadena2);

/**
 * Obtener la representación de un número entero en la base numérica
 * indicada. No se incluyen afijos que identifiquen la base.
 *
 * @param cadena     bloque de memoria donde generar la cadena.
 * @param numero     número a representar.
 * @param base       base numérica con la cual representar, entre 2 y 10.
 * @param ancho_min  cantidad mínima de caracteres (se rellena con el
 *                   caracter de relleno configurado de ser necesario).
 * @return una subcadena de `cadena', donde empieza la representación del
 *         número.
 *
 * @sa cadena_desde_numero_hexa
 */
char        *cadena_desde_numero(char        *cadena,
                                 unsigned int numero,
                                 const uint8  base,
                                 const uint8  ancho_min);

/**
 * Obtener la representación de un número entero en base hexadecimal. No se
 * incluyen afijos que identifiquen la base, como 0x o h.
 *
 * @param cadena     bloque de memoria donde generar la cadena.
 * @param numero     número a representar.
 * @param ancho_min  cantidad mínima de caracteres (se rellena con el
 *                   caracter de relleno configurado de ser necesario).
 * @return una subcadena de `cadena', donde empieza la representación del
 *         número.
 *
 * @sa cadena_desde_numero
 */
char        *cadena_desde_numero_hexa(char        *cadena,
                                      unsigned int numero,
                                      const uint8  ancho_min);

/**
 * Obtener un número entero a partir de una representación textual del mismo
 * en una base numérica dada. No debe haber prefijos en la representación.
 *
 * @param cadena  cadena con la representación del número.
 * @param base    base numérica con la cual está representado el número
 *                (entre 2 y 10).
 * @param numero  puntero donde guardar el número representado.
 * @return verdadero si hubo éxito. falso si no.
 */
bool         cadena_hacia_numero(const char   *cadena,
                                 const uint8   base,
                                 unsigned int *numero);

/**
 * Comprobar si una cadena es prefijo de otra; en otras palabras, comprobar
 * si todos los caracteres de una cadena coinciden con los caracteres al
 * principio de otra, independientemente de lo que esta tenga al final.
 */
bool         cadena_prefijo(const char *cadena,
                            const char *prefijo);

/**
 * Copiar el contenido de una cadena en otra, excluyendo los espacios al
 * principio y al final.
 *
 * @return cantidad de caracteres copiados (excluyendo el caracter nulo).
 */
unsigned int cadena_recortar(char       *destino,
                             const char *origen);

/**
 * Obtener el caracter de relleno.
 *
 * @sa cadena_cambiar_relleno
 */
char         cadena_relleno(void);


#endif
