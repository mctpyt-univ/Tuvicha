/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#ifdef DEPURACION

#include "asegurar.h"
#include "../cpu.h"
#include "../pantalla.h"


void asegurar_interno(const int   resultado,
                      const char *expresion,
                      const char *funcion)
{
    if (resultado)
        return;

    /* Empezamos a colgar el sistema. */
    cpu_apagar_interrupciones();
    /* Mostramos un lindo cartel de error fatal. */
    pantalla_escribir("\n========================================"
                      "========================================");
    pantalla_resaltar();
    pantalla_escribir("    ERROR FATAL\n\n");
    pantalla_resaltar();
    pantalla_escribir("    En la funcion `");
    pantalla_escribir(funcion);
    pantalla_escribir("':\n    no se cumple la expresion `");
    pantalla_escribir(expresion);
    pantalla_escribir("'.\n    Se colgo el sistema."
                      "\n========================================"
                      "========================================");
    /* Terminamos de colgar el sistema. */
    pantalla_desactivar_cursor();
    cpu_dormir();
}


#endif
