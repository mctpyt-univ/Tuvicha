/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "tiempo.h"
#include "cadena.h"
#include "../pantalla.h"


#define SEPARADOR_HORA     ':'
#define SEPARADOR_FECHA    '-'

void tiempo_escribir_hora(const TiempoHora hora)
{
    const char relleno = cadena_relleno();

    cadena_cambiar_relleno('0');
    pantalla_escribir_numero(hora.hora, 10, 2);
    pantalla_escribir_cr(SEPARADOR_HORA);
    pantalla_escribir_numero(hora.minuto, 10, 2);
    pantalla_escribir_cr(SEPARADOR_HORA);
    pantalla_escribir_numero(hora.segundo, 10, 2);
    cadena_cambiar_relleno(relleno);
}

void tiempo_escribir_fecha(const TiempoFecha fecha)
{
    const char relleno = cadena_relleno();

    cadena_cambiar_relleno('0');
    pantalla_escribir_numero(fecha.anio, 10, 4);
    pantalla_escribir_cr(SEPARADOR_FECHA);
    pantalla_escribir_numero(fecha.mes, 10, 2);
    pantalla_escribir_cr(SEPARADOR_FECHA);
    pantalla_escribir_numero(fecha.dia, 10, 2);
    cadena_cambiar_relleno(relleno);
}
