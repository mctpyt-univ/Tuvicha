/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Módulo que implementa un anillo de cadenas de caracteres ASCII de       *
 * longitud fija terminadas en nulo. Un anillo o vector circular es una    *
 * estructura de datos que permite almacenar una cantidad fija `n' de      *
 * elementos y, una vez excedida tal cantidad, sobrescribir los elementos  *
 * por orden de antigüedad; de esta forma, en todo momento se tiene acceso *
 * al elemento actual y a los `n - 1' elementos anteriores (en caso de     *
 * haberlos).                                                              *
 *                                                                         *
 * Salvo que se especifique lo contrario, todo puntero pasado como         *
 * argumento debe ser no nulo.                                             *
 ***************************************************************************/
#ifndef TUVICHA__UTILIDADES_ANILLO__H
#define TUVICHA__UTILIDADES_ANILLO__H


#include "comun.h"


/**
 * Cantidad de cadenas contenidas en el anillo.
 */
#define ANILLO_LONGITUD            8

/**
 * Longitud de cada una de las cadenas que representan los elementos del
 * anillo.
 */
#define ANILLO_CADENA_LONGITUD    40

typedef struct {
    char  v[ANILLO_CADENA_LONGITUD];
    /** Cantidad de caracteres ocupados en la cadena. */
    uint8 ocupado;
} AnilloElemento;

typedef struct {
    /** Elementos del anillo. */
    AnilloElemento a[ANILLO_LONGITUD];
    /** Posición actual en el anillo. */
    uint8          posicion;
} Anillo;


/**
 * Agregar caracter en la cadena actual de un anillo. Debe asegurarse que
 * haya lugar en la misma antes de llamar a esta función.
 *
 * @param anillo    anillo sobre el cual trabajar.
 * @param caracter  caracter a ingresar en la cadena del anillo.
 */
void        anillo_agregar_cr(Anillo    *anillo,
                              const char caracter);

/**
 * Borrar, en caso de haberlo, el último caracter ingresado en la cadena
 * actual de un anillo.
 *
 * @param anillo  anillo sobre el cual trabajar.
 * @return verdadero si se borró un caracter, falso si no.
 */
bool        anillo_borrar_cr(Anillo *anillo);

/**
 * Obtener una de las cadenas de un anillo.
 *
 * @param anillo  anillo sobre el cual trabajar.
 * @param indice  índice relativo a la cadena actual que indica cuál cadena
 *                del anillo considerar.
 * @return puntero a la cadena correspondiente.
 */
const char *anillo_cadena(Anillo     *anillo,
                          const uint8 indice);

/**
 * Girar un anillo de modo que la cadena que sigue pase a ser la actual.
 *
 * @param anillo  anillo sobre el cual trabajar.
 */
void        anillo_girar(Anillo *anillo);

/**
 * Controlar si hay lugar para ingresar más caracteres en una de las cadenas
 * de un anillo.
 *
 * @param anillo  anillo sobre el cual trabajar.
 * @param indice  índice relativo a la cadena actual que indica cuál cadena
 *                del anillo considerar.
 * @return verdadero si hay lugar, falso si no.
 */
bool        anillo_hay_lugar(const Anillo *anillo,
                             const uint8   indice);

/**
 * Obtener la cantidad de caracteres ingresados en una de las cadenas de un
 * anillo.
 *
 * @param anillo  anillo sobre el cual trabajar.
 * @param indice  índice relativo a la cadena actual que indica cuál cadena
 *                del anillo considerar.
 * @return cantidad de caracteres ingresados.
 */
uint8       anillo_ocupado(const Anillo *anillo,
                           const uint8   indice);

/**
 * Reemplazar el contenido de la cadena actual de un anillo por el de otra
 * cadena.
 *
 * @param anillo  anillo sobre el cual trabajar.
 * @param cadena  cadena cuyo contenido copiar en la cadena del anillo.
 */
void        anillo_reemplazar(Anillo     *anillo,
                              const char *cadena);


#endif
