/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "cadena.h"
#include "asegurar.h"


static const char mapa_hexa[] = "0123456789ABCDEF";

static char       relleno = ' ';

void cadena_a_mayuscula(char *cadena)
{
    asegurar(cadena != NULL);

    for (; *cadena; ++cadena)
        if (*cadena >= 'a' && *cadena <= 'z')
            *cadena -= 'a' - 'A';
}

void cadena_cambiar_relleno(const char relleno_nuevo)
{
    relleno = relleno_nuevo;
}

char *cadena_componente(char      *cadena_dada,
                        const char separador)
{
    static char *cadena = NULL;
    char        *componente;

    if (cadena_dada)
        cadena = cadena_dada;
    else if (!cadena)
        return NULL;

    /* Ignoramos componentes vacíos al principio. */
    for (; *cadena == separador && *cadena != '\0'; ++cadena);
    if (*cadena == '\0') {
        cadena = NULL;
        return NULL;
    }

    componente = cadena;
    for (; *cadena != separador && *cadena != '\0'; ++cadena);
    if (*cadena)
        *cadena++ = '\0';
    else
        cadena = NULL;
    return componente;
}

unsigned int cadena_copiar(char       *destino,
                           const char *origen)
{
    unsigned int i;

    asegurar(destino != NULL);
    asegurar(origen != NULL);

    for (i = 0; origen[i]; ++i)
        destino[i] = origen[i];
    destino[i] = '\0';
    return i;
}

bool cadena_iguales(const char *cadena1,
                    const char *cadena2)
{
    unsigned int i;

    asegurar(cadena1 != NULL);
    asegurar(cadena2 != NULL);

    i = 0;
    do
        if (cadena1[i] != cadena2[i])
            return false;
    while (cadena1[i++]);

    return true;
}

char *cadena_desde_numero(char        *cadena,
                          unsigned int numero,
                          const uint8  base,
                          const uint8  ancho_min)
{
    char *cadena_p;

    asegurar(cadena != NULL);
    asegurar(base >= 2);
    asegurar(base <= 10);

    cadena_p    = cadena + sizeof cadena - 1;
    *cadena_p-- = '\0';
    if (numero == 0)
        /* En caso de que el número sea 0, debe haber por lo menos un dígito
           0. */
        *cadena_p-- = '0';
    else
        /* Vamos agregando dígitos de atrás hacia adelante. */
        for (; numero; --cadena_p, numero /= base)
            *cadena_p = '0' + numero % base;

    while (cadena + sizeof cadena - 2 - cadena_p < ancho_min)
        *cadena_p-- = relleno;
    return cadena_p + 1;
}

char *cadena_desde_numero_hexa(char        *cadena,
                               unsigned int numero,
                               const uint8  ancho_min)
{
    char *cadena_p;

    asegurar(cadena != NULL);

    cadena_p    = cadena + sizeof cadena - 1;
    *cadena_p-- = '\0';
    if (numero == 0)
        /* En caso de que el número sea 0, debe haber por lo menos un dígito
           0. */
        *cadena_p-- = '0';
    else
        /* Vamos agregando dígitos de atrás hacia adelante. */
        for (; numero; --cadena_p, numero >>= 4)
            *cadena_p = mapa_hexa[numero & 0xF];

    while (cadena + sizeof cadena - 2 - cadena_p < ancho_min)
        *cadena_p-- = relleno;
    return cadena_p + 1;
}

bool cadena_hacia_numero(const char   *cadena,
                         const uint8   base,
                         unsigned int *numero)
{
    asegurar(cadena != NULL);
    asegurar(base >= 2);
    asegurar(base <= 10);
    asegurar(numero != NULL);

    for (*numero = 0; *cadena != '\0'; ++cadena) {
        if (*cadena < '0' || *cadena >= '0' + base)
            return false;
        *numero = *numero * base + *cadena - '0';
    }
    return true;
}

bool cadena_prefijo(const char *cadena,
                    const char *prefijo)
{
    unsigned int i;

    asegurar(cadena != NULL);
    asegurar(prefijo != NULL);

    for (i = 0; prefijo[i]; ++i)
        if (cadena[i] != prefijo[i])
            return false;
    return true;
}

unsigned int cadena_recortar(char       *destino,
                             const char *origen)
{
    unsigned int i;

    asegurar(destino != NULL);
    asegurar(origen != NULL);

    /* Nos salteamos los espacios al principio. */
    for (; *origen == ' '; ++origen);

    for (i = 0; origen[i]; ++i)
        destino[i] = origen[i];
    destino[i] = '\0';

    /* Borramos los espacios al final. */
    while (i != 0 && destino[i - 1] == ' ')
        destino[--i] = '\0';

    return i;
}

char cadena_relleno(void)
{
    return relleno;
}
