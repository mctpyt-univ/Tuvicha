/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Utilidades para manejar bloques de memoria.                             *
 ***************************************************************************/
#ifndef TUVICHA__UTILIDADES_MEMORIA__H
#define TUVICHA__UTILIDADES_MEMORIA__H


/**
 * Copiar los bytes de un bloque de memoria en otro bloque de memoria.
 *
 * @param destino  bloque de destino.
 * @param origen   bloque de origen.
 * @param tamanio  cantidad de bytes a copiar.
 */
void memoria_copiar(void        *destino,
                    const void  *origen,
                    unsigned int tamanio);

/**
 * Inicializar los bytes de un bloque de memoria con un mismo valor constante.
 */
void memoria_inicializar(void        *bloque,
                         char         valor,
                         unsigned int tamanio);

/**
 * Inicializar las palabras (2 bytes) de un bloque de memoria con un mismo
 * valor constante.
 */
void memoria_inicializar_2(void        *bloque,
                           short        valor,
                           unsigned int tamanio);


#endif
