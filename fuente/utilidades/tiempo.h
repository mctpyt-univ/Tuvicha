/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Utilidades para manejar la representación del tiempo (fechas y horas).  *
 ***************************************************************************/
#ifndef TUVICHA__UTILIDADES_TIEMPO__H
#define TUVICHA__UTILIDADES_TIEMPO__H


#include "comun.h"


typedef struct {
    uint8 hora;
    uint8 minuto;
    uint8 segundo;
} TiempoHora;

typedef struct {
    uint16 anio;
    uint8  mes;
    uint8  dia;
} TiempoFecha;

void tiempo_escribir_hora(const TiempoHora hora);

void tiempo_escribir_fecha(const TiempoFecha fecha);


#endif
