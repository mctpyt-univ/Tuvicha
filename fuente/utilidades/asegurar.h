/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Utilidades para hacer comprobaciones de prerrequisitos al depurar.      *
 ***************************************************************************/
#ifndef TUVICHA__UTILIDADES_ASEGURAR__H
#define TUVICHA__UTILIDADES_ASEGURAR__H

#ifdef DEPURACION


#define asegurar(expresion) \
    asegurar_interno((const int) (expresion), #expresion, __func__)

void asegurar_interno(const int   resultado,
                      const char *expresion,
                      const char *funcion);

#else

#define asegurar(expresion)


#endif
#endif
