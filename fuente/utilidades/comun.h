/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Tipos y macros comunes.                                                 *
 ***************************************************************************/
#ifndef TUVICHA__UTILIDADES_COMUN__H
#define TUVICHA__UTILIDADES_COMUN__H


#define NULL     ((void *) 0)
#define false    ((_Bool)  0)
#define true     ((_Bool)  1)

typedef _Bool bool;

typedef char               int8;
typedef short              int16;
typedef int                int32;
typedef long long          int64;
typedef unsigned char      uint8;
typedef unsigned short     uint16;
typedef unsigned int       uint32;
typedef unsigned long long uint64;

/** Obtener la longitud de un vector. */
#define LONGITUD(v)    (sizeof (v) / sizeof *(v))

/**
 * Generar una máscara que, al aplicarse a un entero, mantenga una cierta
 * cantidad de bits menos significativos y deja los demás en 0.
 *
 * @param n  cantidad de bits menos significativos a mantener.
 */
#define MANTENER_BITS(n)    \
    ((unsigned int) -1 >> (sizeof (unsigned int) * 8 - (n)))


#endif
