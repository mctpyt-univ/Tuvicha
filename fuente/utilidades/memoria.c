/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "memoria.h"
#include "asegurar.h"


void memoria_copiar(void        *origen,
                    const void  *destino,
                    unsigned int tamanio)
{
    unsigned int i;

    asegurar(origen);
    asegurar(destino);

    for (i = 0; i < tamanio; ++i)
        ((char *) origen)[i] = ((const char *) destino)[i];
}

void memoria_inicializar(void        *bloque,
                         char         valor,
                         unsigned int tamanio)
{
    unsigned int i;

    asegurar(bloque);

    for (i = 0; i < tamanio; ++i)
        ((char *) bloque)[i] = valor;
}

void memoria_inicializar_2(void        *bloque,
                           short        valor,
                           unsigned int tamanio)
{
    unsigned int i;

    asegurar(bloque);

    for (i = 0; i < tamanio; ++i)
        ((short *) bloque)[i] = valor;
}
