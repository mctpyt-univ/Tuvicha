/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "anillo.h"
#include "asegurar.h"
#include "cadena.h"


static uint8 calcular_indice(const Anillo *anillo,
                             uint8         indice)
{
    asegurar(anillo != NULL);

    if (indice == 0)
        return anillo->posicion;

    indice %= ANILLO_LONGITUD;
    if (indice > anillo->posicion)
        indice -= ANILLO_LONGITUD;
    return anillo->posicion - indice;
}

void anillo_agregar_cr(Anillo    *anillo,
                       const char caracter)
{
    char *cadena;

    asegurar(anillo_hay_lugar(anillo, 0));

    cadena = anillo->a[anillo->posicion].v;
    cadena[anillo->a[anillo->posicion].ocupado++] = caracter;
    cadena[anillo->a[anillo->posicion].ocupado]   = '\0';
}

bool anillo_borrar_cr(Anillo *anillo)
{
    asegurar(anillo != NULL);

    if (anillo->a[anillo->posicion].ocupado) {
        anillo->a[anillo->posicion].v
            [--anillo->a[anillo->posicion].ocupado] = '\0';
        return true;
    } else
        return false;
}

const char *anillo_cadena(Anillo     *anillo,
                          const uint8 indice)
{
    return anillo->a[calcular_indice(anillo, indice)].v;
}

void anillo_girar(Anillo *anillo)
{
    asegurar(anillo != NULL);

    /* Si la cadena actual está vacía, no giramos. */
    if (anillo->a[anillo->posicion].ocupado == 0)
        return;

    if (anillo->posicion < ANILLO_LONGITUD - 1)
        ++anillo->posicion;
    else
        anillo->posicion = 0;
    anillo->a[anillo->posicion].ocupado = 0;
    anillo->a[anillo->posicion].v[0]    = '\0';
}

bool anillo_hay_lugar(const Anillo *anillo,
                      const uint8   indice)
{
    const uint8 ocupado = anillo->a[calcular_indice(anillo, indice)].ocupado;

    /* El -1 al final nos asegura que siempre se deje disponible lugar para
       el caracter nulo. */
    return ocupado < sizeof anillo->a->v - 1;
}

uint8 anillo_ocupado(const Anillo *anillo,
                     const uint8   indice)
{
    return anillo->a[calcular_indice(anillo, indice)].ocupado;
}

void anillo_reemplazar(Anillo     *anillo,
                       const char *cadena)
{
    asegurar(anillo != NULL);

    anillo->a[anillo->posicion].ocupado =
        cadena_copiar(anillo->a[anillo->posicion].v, cadena);
}
