/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "cpu.h"
#include "puerto.h"
#include "teclado.h"
#include "utilidades/comun.h"


/**
 * Cadena de identificación de la CPU.
 */
static union {
    uint32 vn[3];
    char   vc[13];
} id;

int _cpu_id_disponible(void);


void cpu_apagar_interrupciones(void)
{
    __asm__ __volatile__("cli");
}

const char *cpu_id(void)
{
    uint32 max;

    /* Solo se pide la cadena a la CPU la primera vez, y se la guarda para
       futuras ocasiones. */
    if (id.vc[0])
        return id.vc;

    /* Vemos si está disponible la instrucción para obtener la cadena. */
    if (!_cpu_id_disponible())
        return NULL;

    __asm__ __volatile__("cpuid"
                         : "=a" (max),      "=b" (id.vn[0]),
                           "=d" (id.vn[1]), "=c" (id.vn[2])
                         : "a" (0));
    return id.vc;
}

void cpu_dormir(void)
{
    for (;;)
        /* La instrucción `hlt' es más eficiente que un mero bucle infinito.
           Pero se la debe envolver en un bucle porque si se recibe una
           interrupción, al terminar de manejarla, la CPU procederá a
           ejecutar lo que le siga a esta instrucción `hlt'. */
        __asm__ __volatile__("hlt");
}

void cpu_reiniciar(void)
{
    /* Esperamos a que se vacíe el búfer de entrada del controlador de
       teclado. */
    while (puerto_leer(TECLADO_CONTROLADOR_PUERTO) & 0x2);

    /* Mandamos al controlador de teclado el comando para reiniciar. */
    puerto_escribir(TECLADO_CONTROLADOR_PUERTO, 0xFE);
}
