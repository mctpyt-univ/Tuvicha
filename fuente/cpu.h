/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Módulo para efectuar ciertas acciones básicas en la CPU x86.            *
 ***************************************************************************/
#ifndef TUVICHA__CPU__H
#define TUVICHA__CPU__H


/**
 * Apagar interrupciones en la CPU. Llamar a esta función y seguidamente a
 * `cpu_dormir' produce un cuelgue del procesador.
 */
void        cpu_apagar_interrupciones(void);

/**
 * Obtener, en caso de estar disponible, la cadena de texto de identificación
 * de la CPU.
 */
const char *cpu_id(void);

/**
 * Dejar la CPU durmiendo. Si las interrupciones están activadas, la misma
 * despertará al recibir una; de lo contrario, quedará colgada
 * indefinidamente.
 */
void        cpu_dormir(void);

/**
 * Reiniciar la CPU, por medio del controlador de teclado.
 */
void        cpu_reiniciar(void);


#endif
