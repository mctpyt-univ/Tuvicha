/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Módulo para manejar las particiones del MBR de un disco duro. Solo se   *
 * reconocen las hasta 4 particiones que están directamente en el MBR, es  *
 * decir, las primarias.                                                   *
 *                                                                         *
 * Las particiones se numeran a partir de 1. El valor 0, por su parte,     *
 * representa el disco entero.                                             *
 ***************************************************************************/
#ifndef TUVICHA__MBR__H
#define TUVICHA__MBR__H


#include "utilidades/comun.h"


#define MBR_CANTIDAD_PARTICIONES    4

typedef enum {
    MBR_TIPO_PARTICION_INEXISTENTE   = 0x00,
    MBR_TIPO_PARTICION_FAT12         = 0x01,
    MBR_TIPO_PARTICION_FAT16_CHICO   = 0x04,
    MBR_TIPO_PARTICION_EXTENDIDA     = 0x05,
    MBR_TIPO_PARTICION_FAT16         = 0x06,
    MBR_TIPO_PARTICION_FAT32         = 0x0B,
    MBR_TIPO_PARTICION_FAT32_LBA     = 0x0C,
    MBR_TIPO_PARTICION_FAT16_LBA     = 0x0E,
    MBR_TIPO_PARTICION_EXTENDIDA_LBA = 0x0F,
    MBR_TIPO_PARTICION_GNULINUX      = 0x83,
    MBR_TIPO_PARTICION_BSD           = 0xA5
} MBRTipoParticion;

typedef struct {
    uint8  arrancable;
    uint8  cabezal_inicial;
    uint8  sector_inicial;
    uint16 cilindro_inicial;
    uint8  id_sistema;
    uint8  cabezal_final;
    uint8  sector_final;
    uint16 cilindro_final;
    uint32 lba_inicial;
    uint32 cantidad_sectores;
} MBRParticion;


/**
 * Obtener el número de la partición seleccionada actualmente.
 */
uint8        mbr_actual(void);

/**
 * Cambiar la selección actual de partición.
 */
bool         mbr_cambiar_actual(uint8 indice);

/**
 * Leer el MBR del disco actual y cargarlo en RAM en caso de que no esté ya
 * cargado o de que se fuerce a hacerlo.
 *
 * @param forzar  forzar a cargar el MBR aun si ya está cargado.
 * @return verdadero si se pudo cargar con éxito el MBR del disco, falso si no.
 */
bool         mbr_cargar(const bool forzar);

/**
 * Controlar la firma de autenticidad en el MBR cargado.
 */
bool         mbr_controlar_firma(void);

/**
 * Obtener una estructura con toda la información de una partición del MBR
 * directamente accesible.
 *
 * @param indice  índice de la partición (de 1 a 4).
 */
MBRParticion mbr_particion(const uint8 indice);

/**
 * Obtener una cadena de descripción del tipo de partición para una partición
 * dada. Los tipos de partición reconocidos son los de la enumeración
 * `MBRTipoParticion'; para cualquier otro se devuelve una cadena que indica
 * que el tipo es desconocido.
 */
const char  *mbr_tipo_particion(const MBRParticion particion);


#endif
