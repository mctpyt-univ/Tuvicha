/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "comandos.h"
#include "lineacomando.h"
#include "comandos/ayuda.h"
#include "comandos/disco.h"
#include "comandos/fat32.h"
#include "comandos/limpiar.h"
#include "comandos/luces.h"
#include "comandos/particion.h"
#include "comandos/reiniciar.h"
#include "comandos/sistema.h"
#include "comandos/teclado.h"
#include "comandos/tuvicha.h"


void comandos_instalar(void)
{
    LINEACOMANDO_INSTALAR_COMANDO(ayuda);
    LINEACOMANDO_INSTALAR_COMANDO(disco);
    LINEACOMANDO_INSTALAR_COMANDO(fat32);
    LINEACOMANDO_INSTALAR_COMANDO(limpiar);
    LINEACOMANDO_INSTALAR_COMANDO(luces);
    LINEACOMANDO_INSTALAR_COMANDO(particion);
    LINEACOMANDO_INSTALAR_COMANDO(reiniciar);
    LINEACOMANDO_INSTALAR_COMANDO(sistema);
    LINEACOMANDO_INSTALAR_COMANDO(teclado);
    LINEACOMANDO_INSTALAR_COMANDO(tuvicha);
    lineacomando_establecer_comando_ayuda("ayuda");
}
