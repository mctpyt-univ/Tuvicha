/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "cga.h"
#include "puerto.h"
#include "utilidades/memoria.h"


/* Pendiente: obtener los valores de estas macros de la BDA (“BIOS Data
   Area”), si es que vale la pena.
   http://stanislavs.org/helpcc/6845.html */
#define COLUMNAS         80u
#define FILAS            25u
#define CGA_PUERTO    0x3D4


/**
 * Búfer de la memoria de texto de video.
 */
static uint16 *bufer = (uint16 *) 0xB8000;

static uint16  cursor, cursor_minimo;


/**
 * Desplazar la pantalla una línea hacia arriba y dejar el cursor al
 * principio de la última línea.
 */
static inline void desplazar(const uint8 atributo)
{
    unsigned int i;

    for (i = cursor_minimo / COLUMNAS + 1; i < FILAS; ++i)
        memoria_copiar(bufer + COLUMNAS * (i - 1), bufer + COLUMNAS * i,
                       COLUMNAS * 2);
    memoria_inicializar_2(bufer + COLUMNAS * (FILAS - 1),
                          atributo << 8, COLUMNAS);
    cursor = COLUMNAS * (FILAS - 1);
}

void cga_borrar(const uint8 atributo)
{
    if (cursor == cursor_minimo)
        return;

    bufer[--cursor] = ' ' | atributo << 8;
    /* Pendiente: pensar qué hacemos con el borrado de saltos de línea. */
}

void cga_desactivar_cursor(void)
{
    puerto_escribir(CGA_PUERTO,     10);
    puerto_escribir(CGA_PUERTO + 1, 0x20);
}

void cga_escribir(const char  caracter,
                  const uint8 atributo)
{
    if (cursor >= COLUMNAS * FILAS)
        desplazar(atributo);

    if (caracter == '\n')
        cursor += COLUMNAS - cursor % COLUMNAS;
    else
        bufer[cursor++] = caracter | atributo << 8;
}

void cga_establecer_cursor_minimo(void)
{
    cursor_minimo = cursor;
}

void cga_llenar_fila(const uint8 atributo)
{
    const uint8 cursor_final = cursor + COLUMNAS - cursor % COLUMNAS;

    while (cursor != cursor_final)
        bufer[cursor++] = atributo << 8;
}

void cga_limpiar(const uint8 atributo)
{
    memoria_inicializar_2(bufer + cursor_minimo, atributo << 8,
                          COLUMNAS * FILAS);
    cursor = cursor_minimo;
}

void cga_sincronizar_cursor(void)
{
    puerto_escribir(CGA_PUERTO,     14);
    puerto_escribir(CGA_PUERTO + 1, (int8) (cursor >> 8));
    puerto_escribir(CGA_PUERTO,     15);
    puerto_escribir(CGA_PUERTO + 1, (int8) cursor);
}
