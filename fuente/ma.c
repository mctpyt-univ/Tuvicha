/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "ma.h"
#include "utilidades/asegurar.h"


typedef struct {
    uint32        banderas;
    uint32        memoria_baja;
    uint32        memoria_alta;
    MADispositivo dispositivo_arranque;
    const char   *linea_comando;
    uint32        modulos_cantidad;
    uint32        modulos_direccion;
    uint32        simbolos[3];
    uint32        mapa_memoria_longitud;
    uint32        discos_longitud;
    uint32        discos_direccion;
    uint32        tabla_config;
    const char   *nombre_arrancador;
    uint32        tabla_apm;
    uint32        vbe[6];
} Info;

static const Info *info;


uint32 ma_banderas(void)
{
    asegurar(info != NULL);

    return info->banderas;
}

bool ma_cantidad_modulos(uint32 *cantidad_modulos)
{
    asegurar(info != NULL);

    if (!(info->banderas & 8))
        return false;

    *cantidad_modulos = info->modulos_cantidad;
    return true;
}

bool ma_dispositivo(MADispositivo *dispositivo)
{
    asegurar(info != NULL);

    if (!(info->banderas & 2))
        return false;

    *dispositivo = info->dispositivo_arranque;
    return true;
}

bool ma_inicializado(void)
{
    return info != NULL;
}

void ma_inicializar(const int32 *info_nueva)
{
    asegurar(info_nueva != NULL);

    info = (const Info *) info_nueva;
}

const char *ma_linea_comando(void)
{
    asegurar(info != NULL);

    return info->banderas & 4 ? info->linea_comando : NULL;
}

bool ma_memoria(uint32 *memoria_baja,
                uint32 *memoria_alta)
{
    asegurar(info != NULL);

    if (!(info->banderas & 1))
        return false;

    *memoria_baja = info->memoria_baja;
    *memoria_alta = info->memoria_alta;
    return true;
}

const char *ma_nombre_arrancador(void)
{
    asegurar(info != NULL);

    return info->banderas & 0x200 ? info->nombre_arrancador : NULL;
}
