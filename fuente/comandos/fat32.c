/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "fat32.h"
#include "../disco.h"
#include "../fat32.h"
#include "../lineacomando.h"
#include "../mbr.h"
#include "../pantalla.h"
#include "../utilidades/cadena.h"
#include "../utilidades/tiempo.h"


#define ACCION_INFO               "info"
#define ACCION_LISTAR             "listar"
#define ACCION_LISTAR_COMPACTO    "listar-compacto"

const char comando_fat32_descripcion[] =
    "navegar o inspeccionar un sistema de archivos FAT32";

const char comando_fat32_args[] = "(" ACCION_INFO               "|"
                                  ACCION_LISTAR          " <ruta>|"
                                  ACCION_LISTAR_COMPACTO " <ruta>)";


static inline bool cargar_lba_inicial(uint32 *lba_inicial)
{
    const uint8  indice = mbr_actual();
    MBRParticion particion;

    if (indice == 0) {
        /* Usamos el disco entero. */
        *lba_inicial = 0;
        return true;
    }

    /* Obtenemos la dirección LBA inicial de la partición actual. */

    if (!mbr_cargar(false)) {
        pantalla_escribir("Error: no se pudo cargar el MBR para obtener "
                          "informacion de la particion.\n");
        return false;
    }

    particion = mbr_particion(indice);
    if (!particion.id_sistema) {
        pantalla_escribir("Error: no existe la particion ");
        pantalla_escribir_numero(indice, 10, 0);
        pantalla_escribir(".\n");
        return false;
    } else if (particion.id_sistema != MBR_TIPO_PARTICION_FAT32 &&
               particion.id_sistema != MBR_TIPO_PARTICION_FAT32_LBA)
        pantalla_escribir("Advertencia: la particion no es de tipo "
                          "FAT32.\n");

    *lba_inicial = particion.lba_inicial;
    return true;
}

static inline void error_fat32_invalido(void)
{
    pantalla_escribir("Error: la particion o el disco seleccionado no "
                      "contiene un sistema de archivos\nFAT32.\n");
}

static inline void info(void)
{
    uint32    lba_inicial;
    FAT32Info info_fat32;

    if (!cargar_lba_inicial(&lba_inicial))
        return;
    pantalla_escribir("LBA inicial:                        ");
    pantalla_escribir_numero(lba_inicial, 10, 0);
    pantalla_escribir_cr('\n');

    if (!fat32_cargar(lba_inicial, &info_fat32)) {
        error_fat32_invalido();
        return;
    }

    pantalla_escribir("Identificador de OEM:               ");
    pantalla_escribir(info_fat32.id_oem);
    pantalla_escribir("\nSectores por cluster:               ");
    pantalla_escribir_numero(info_fat32.sectores_por_cluster, 10, 0);
    pantalla_escribir("\nSectores reservados:                ");
    pantalla_escribir_numero(info_fat32.sectores_reservados, 10, 0);
    pantalla_escribir("\nCantidad de tablas FAT:             ");
    pantalla_escribir_numero(info_fat32.cantidad_fat, 10, 0);
    pantalla_escribir("\nSectores por tabla FAT:             ");
    pantalla_escribir_numero(info_fat32.sectores_por_fat, 10, 0);
    pantalla_escribir("\nPrimer cluster del directorio raiz: ");
    pantalla_escribir_numero(info_fat32.primer_cluster_dir_raiz, 10, 0);

    pantalla_escribir("\nEspacio libre:                      ");
    if (info_fat32.clusteres_libres == (uint32) -1)
        pantalla_escribir("desconocido\n");
    else {
        const uint32 mib_libres = info_fat32.clusteres_libres *
                                  info_fat32.sectores_por_cluster >> 11;
        pantalla_escribir_numero(mib_libres, 10, 0);
        pantalla_escribir(" MiB\n");
    }
}

static inline bool obtener_entrada_de_dir(FAT32Archivo **dir_p,
                                          FAT32Archivo  *dir,
                                          char          *ruta)
{
    char        *ruta_p;
    const char  *componente;
    FAT32Archivo archivo;

    cadena_a_mayuscula(ruta);
    ruta_p = ruta;
    *dir_p = NULL;

proximo_componente:
    while ((componente = cadena_componente(ruta_p, '/'))) {
        ruta_p = NULL;
        while (fat32_navegar(*dir_p, &archivo)) {
            if (cadena_iguales(archivo.nombre, componente))
                if (archivo.atributos.a.directorio) {
                    *dir   = archivo;
                    *dir_p = dir;
                    goto proximo_componente;
                } else {
                    pantalla_escribir("Error: el archivo indicado no es un "
                                      "directorio.\n");
                    return false;
                }
        }
        pantalla_escribir("Error: no se encontro el directorio `");
        pantalla_escribir(componente);
        pantalla_escribir("' en `");
        pantalla_escribir(*dir_p ? dir->nombre : "/");
        pantalla_escribir("'.\n");
        return false;
    }
    return true;
}

static inline void listar(char      *ruta,
                          const bool compacto)
{
    uint32       lba_inicial;
    FAT32Archivo archivo, dir, *dir_p;

    if (!cargar_lba_inicial(&lba_inicial))
        return;
    if (!fat32_cargar(lba_inicial, NULL)) {
        error_fat32_invalido();
        return;
    }

    if (ruta) {
        pantalla_escribir("Listando directorio: ");
        pantalla_escribir(ruta);
        pantalla_escribir_cr('\n');
        if (!obtener_entrada_de_dir(&dir_p, &dir, ruta))
            return;
    } else {
        char ruta_raiz[] = "";
        pantalla_escribir("Listando directorio: /\n");
        if (!obtener_entrada_de_dir(&dir_p, &dir, ruta_raiz))
            return;
    }

    if (compacto) {
        while(fat32_navegar(dir_p, &archivo)) {
            pantalla_escribir(archivo.nombre);
            pantalla_escribir(", ");
        }
        pantalla_escribir_cr('\n');
    }
    else {
        pantalla_escribir(".-------------------------------"
                          "------------------------------.\n"
                          "|    \aNOMBRE\a    | \aATRIBS\a "
                          "| \aTAMANIO (KiB)\a | \aFECHA/HORA MODIFIC\a  |\n"
                          "|--------------+--------+-------"
                          "--------+---------------------|\n");
        while (fat32_navegar(dir_p, &archivo)) {
            pantalla_escribir("| ");
            pantalla_escribir_relleno(archivo.nombre,
                                      sizeof archivo.nombre - 1);
            pantalla_escribir(" | ");
            pantalla_escribir_cr(archivo.atributos.a.archivar    ? 'A' :
                                                                   '-');
            pantalla_escribir_cr(archivo.atributos.a.directorio  ? 'D' :
                                                                   '-');
            pantalla_escribir_cr(archivo.atributos.a.idvolumen   ? 'I' :
                                                                   '-');
            pantalla_escribir_cr(archivo.atributos.a.sistema     ? 'S' :
                                                                   '-');
            pantalla_escribir_cr(archivo.atributos.a.oculto      ? 'O' :
                                                                   '-');
            pantalla_escribir_cr(archivo.atributos.a.sololectura ? 'L' :
                                                                   '-');
            pantalla_escribir(" |    ");
            pantalla_escribir_numero(archivo.tamanio >> 10, 10, 7);
            pantalla_escribir("    | ");
            tiempo_escribir_fecha(archivo.fecha);
            pantalla_escribir_cr(' ');
            tiempo_escribir_hora(archivo.hora);
            pantalla_escribir(" |\n");
        }
        pantalla_escribir("`-------------------------------"
                          "------------------------------'\n");
    }
}

void comando_fat32(void)
{
    const char *accion = lineacomando_arg();

    if (!accion) {
        pantalla_escribir("Error: se debe indicar una accion: "
                          "`" ACCION_INFO            "', "
                          "`" ACCION_LISTAR          "' o "
                          "`" ACCION_LISTAR_COMPACTO "'.\n");
        return;
    } else if (cadena_iguales(accion, ACCION_INFO))
        info();
    else if (cadena_iguales(accion, ACCION_LISTAR))
        listar(lineacomando_arg(), false);
    else if (cadena_iguales(accion, ACCION_LISTAR_COMPACTO))
        listar(lineacomando_arg(), true);
    else {
        pantalla_escribir("Error: no existe la accion `");
        pantalla_escribir(accion);
        pantalla_escribir("'.\n");
        return;
    }
}
