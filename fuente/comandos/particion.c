/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "particion.h"
#include "../disco.h"
#include "../lineacomando.h"
#include "../mbr.h"
#include "../pantalla.h"
#include "../utilidades/cadena.h"


const char comando_particion_descripcion[] =
    "listar o seleccionar particiones primarias";

const char comando_particion_args[] = "[<numero de particion>|extenso]";


static inline int32 sectores_a_mib(const uint32 sectores)
{
    /*  sectores * 512 / 1024 / 1024  */
    return sectores >> 11;
}

static inline void listar_particiones(const bool extenso)
{
    unsigned int indice;
    unsigned int seleccion;

    /* Leemos el MBR del disco, pero solo en caso de que no lo tengamos
       leído ya. */
    if (!mbr_cargar(false)) {
        pantalla_escribir("Error: no se pudo cargar el MBR.\n");
        return;
    }

    if (!mbr_controlar_firma()) {
        pantalla_escribir("Error: la firma del MBR no es valida.\n");
        return;
    }

    pantalla_escribir("Particiones primarias del disco ");
    pantalla_escribir_numero(disco_actual(), 10, 0);
    pantalla_escribir(":\n");

    /* Mostramos la tabla de particiones contenida en el MBR. */
    if (!extenso)
        pantalla_escribir(".----------------------------"
                          "---------------------------------------------.\n"
                          "| \a#\a | \aARR\a |      \aTIPO\a       "
                          "|   \aINICIO\a   | \aINICIO\a  "
                          "|  \aTAMANIO\a   | \aTAMANIO\a |\n"
                          "|   |     |                 "
                          "| \a(SECTORES)\a |  \a(MiB)\a  "
                          "| \a(SECTORES)\a |  \a(MiB)\a  |\n"
                          "|---+-----+-----------------+"
                          "------------+---------+------------+---------|\n"
                          );

    for (indice = 1; indice <= MBR_CANTIDAD_PARTICIONES; ++indice) {
        const MBRParticion particion = mbr_particion(indice);

        if (extenso) {
            pantalla_resaltar();
            pantalla_escribir("PARTICION ");
            pantalla_escribir_cr('0' + indice);
            pantalla_escribir(": ");
            pantalla_resaltar();
            if (particion.id_sistema == 0) {
                pantalla_escribir("inexistente\n");
                continue;
            }

            if (particion.arrancable)
                pantalla_escribir("no ");
            pantalla_escribir("arrancable, tipo ");
            pantalla_escribir(mbr_tipo_particion(particion));
            pantalla_escribir(" (identificador 0x");
            pantalla_escribir_numero_hexa(particion.id_sistema, 0);

            pantalla_escribir(")\n    Cabezal/sector/cilindro inicial: ");
            pantalla_escribir_numero(particion.cabezal_inicial, 10, 0);
            pantalla_escribir_cr('/');
            pantalla_escribir_numero(particion.sector_inicial, 10, 0);
            pantalla_escribir_cr('/');
            pantalla_escribir_numero(particion.cilindro_inicial, 10, 0);
            pantalla_escribir("\n    Cabezal/sector/cilindro final:   ");
            pantalla_escribir_numero(particion.cabezal_final, 10, 0);
            pantalla_escribir_cr('/');
            pantalla_escribir_numero(particion.sector_final, 10, 0);
            pantalla_escribir_cr('/');
            pantalla_escribir_numero(particion.cilindro_final, 10, 0);

            pantalla_escribir("\n    Sector inicial (en LBA):         ");
            pantalla_escribir_numero(particion.lba_inicial, 10, 0);
            pantalla_escribir(" (");
            pantalla_escribir_numero(sectores_a_mib(particion.lba_inicial),
                                     10, 0);
            pantalla_escribir(" MiB)\n"
                              "    Cantidad de sectores:            ");
            pantalla_escribir_numero(particion.cantidad_sectores, 10, 0);
            pantalla_escribir(" (");
            pantalla_escribir_numero(
                sectores_a_mib(particion.cantidad_sectores), 10, 0);
            pantalla_escribir(" MiB)\n");

        } else {
            unsigned int i;
            const char  *tipo_particion = mbr_tipo_particion(particion);

            pantalla_escribir("| ");
            pantalla_escribir_cr('0' + indice);
            pantalla_escribir(" |  ");
            pantalla_escribir_cr(particion.arrancable ? '*' : ' ');
            pantalla_escribir("  | ");

            pantalla_escribir(tipo_particion);
            for (i = 0; tipo_particion[i]; ++i);
            for (; i < 15; ++i)
                pantalla_escribir_cr(' ');
            pantalla_escribir(" | ");

            pantalla_escribir_numero(particion.lba_inicial, 10, 10);
            pantalla_escribir(" | ");
            pantalla_escribir_numero(sectores_a_mib(particion.lba_inicial),
                                     10, 7);
            pantalla_escribir(" | ");

            pantalla_escribir_numero(particion.cantidad_sectores, 10, 10);
            pantalla_escribir(" | ");
            pantalla_escribir_numero(
                sectores_a_mib(particion.cantidad_sectores), 10, 7);
            pantalla_escribir(" |\n");
        }
    }

    if (!extenso)
        pantalla_escribir(
"`-------------------------------------------------------------------------'\n"
                          );

    pantalla_escribir("Seleccion actual: ");
    if ((seleccion = mbr_actual()) == 0)
        pantalla_escribir("disco entero.\n");
    else {
        pantalla_escribir("particion ");
        pantalla_escribir_cr('0' + seleccion);
        pantalla_escribir(".\n");
    }
}

void comando_particion(void)
{
    const char  *arg = lineacomando_arg();
    unsigned int indice;

    if (arg) {
        if (cadena_iguales(arg, "extenso")) {
            listar_particiones(true);
        } else if (cadena_hacia_numero(arg, 10, &indice)) {
            if (!mbr_cambiar_actual(indice)) {
                pantalla_escribir("Error: no existe la "
                                  "particion ");
                pantalla_escribir(arg);
                pantalla_escribir(".\n");
            }
        } else {
            pantalla_escribir("Error: no existe la accion `");
            pantalla_escribir(arg);
            pantalla_escribir("'.\n");
        }
    } else
        listar_particiones(false);
}
