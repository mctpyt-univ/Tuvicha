/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "disco.h"
#include "../disco.h"
#include "../lineacomando.h"
#include "../mbr.h"
#include "../pantalla.h"
#include "../utilidades/cadena.h"


const char comando_disco_descripcion[] = "listar o seleccionar discos duros";

const char comando_disco_args[] = "[<numero de disco>]";


static inline uint32 sectores_a_mib(const uint32 sectores)
{
    return sectores >> 11;
}

static inline void listar_discos(void)
{
    unsigned int     i;
    const DiscoInfo *info;

    pantalla_escribir("Discos duros:\n");
    for (i = 0; i < 4; ++i) {
        info = disco_info(i);
        pantalla_escribir("    Disco ");
        pantalla_escribir_cr('0' + i);
        pantalla_escribir(": ");
        if (info) {
            pantalla_escribir_numero(sectores_a_mib(info->sectores), 10, 0);
            pantalla_escribir(" MiB (serie ");
            pantalla_escribir(info->serie);
            pantalla_escribir(", firmware ");
            pantalla_escribir(info->firmware);
            pantalla_escribir(", modelo ");
            pantalla_escribir(info->modelo);
            pantalla_escribir(")\n");
        } else
            pantalla_escribir("ausente\n");
    }

    pantalla_escribir("Seleccion actual: disco ");
    pantalla_escribir_numero(disco_actual(), 10, 0);
    pantalla_escribir(".\n");
}

void comando_disco(void)
{
    const char *arg = lineacomando_arg();

    if (arg) {
        unsigned int indice;

        if (!cadena_hacia_numero(arg, 10, &indice)) {
            pantalla_escribir("Error: el argumento no es un numero de "
                              "disco: ");
            pantalla_escribir(arg);
            pantalla_escribir(".\n");
        } else if (disco_cambiar_actual(indice))
            mbr_cambiar_actual(0);
        else {
            pantalla_escribir("Error: no existe el disco ");
            pantalla_escribir(arg);
            pantalla_escribir(".\n");
        }
    } else
        listar_discos();
}
