/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "sistema.h"
#include "../cpu.h"
#include "../ma.h"
#include "../pantalla.h"


const char comando_sistema_descripcion[] =
    "mostrar informacion sobre el hardware y el arranque";

const char comando_sistema_args[] = "";


static inline void mostrar_info_cpu(void)
{
    const char *id_cpu;

    if ((id_cpu = cpu_id())) {
        pantalla_escribir("Informacion del hardware:\n"
                          "    Identificador de CPU:  ");
        pantalla_escribir(id_cpu);
        pantalla_escribir_cr('\n');
    }
}

static inline void mostrar_info_arrancador(void)
{
    uint32        memoria_baja, memoria_alta, cantidad_modulos;
    MADispositivo dispositivo;
    const char   *linea_comando, *nombre_arrancador;

    pantalla_escribir("\nInformacion brindada por el arrancador:\n");

    if (!ma_inicializado()) {
        pantalla_escribir("    No disponible (ya que el nucleo no se cargo "
                          "\n    siguiendo la Especificacion de "
                          "Multiarranque).\n");
        return;
    }

    pantalla_escribir("    Banderas:              0x");
    pantalla_escribir_numero_hexa(ma_banderas(), 0);
    pantalla_escribir_cr('\n');

    if (ma_memoria(&memoria_baja, &memoria_alta)) {
        pantalla_escribir("    Memoria baja:          ");
        pantalla_escribir_numero(memoria_baja, 10, 0);
        pantalla_escribir(" KiB\n    Memoria alta:          ");
        pantalla_escribir_numero(memoria_alta, 10, 0);
        pantalla_escribir(" KiB\n");
    }

    if (ma_dispositivo(&dispositivo)) {
        pantalla_escribir("    Disco arrancado:       0x");
        pantalla_escribir_numero_hexa(dispositivo.disco, 0);
        pantalla_escribir("\n    Particion arrancada:   0x");
        pantalla_escribir_numero_hexa(dispositivo.particion_1, 0);
        pantalla_escribir(", 0x");
        pantalla_escribir_numero_hexa(dispositivo.particion_2, 0);
        pantalla_escribir(", 0x");
        pantalla_escribir_numero_hexa(dispositivo.particion_3, 0);
        pantalla_escribir_cr('\n');
    }

    if ((linea_comando = ma_linea_comando())) {
        pantalla_escribir("    Linea de comando:      ");
        pantalla_escribir(linea_comando);
        pantalla_escribir_cr('\n');
    }

    if (ma_cantidad_modulos(&cantidad_modulos)) {
        pantalla_escribir("    Cantidad de modulos:   ");
        pantalla_escribir_numero(cantidad_modulos, 10, 0);
        pantalla_escribir_cr('\n');
    }

    if ((nombre_arrancador = ma_nombre_arrancador())) {
        pantalla_escribir("    Nombre del arrancador: ");
        pantalla_escribir(nombre_arrancador);
        pantalla_escribir_cr('\n');
    }
}

void comando_sistema(void)
{
    mostrar_info_cpu();
    mostrar_info_arrancador();
}
