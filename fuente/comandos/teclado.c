/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "luces.h"
#include "../lineacomando.h"
#include "../pantalla.h"
#include "../teclado.h"


const char comando_teclado_descripcion[] =
    "listar o seleccionar mapas de teclado";

const char comando_teclado_args[] = "[<mapa de teclado>]";


static inline void listar_mapas(void)
{
    unsigned int i;

    pantalla_escribir("Mapas de teclado disponibles: ");
    for (i = 0; i < teclado_cantidad_mapas(); ++i) {
        pantalla_escribir(teclado_mapa(i));
        pantalla_escribir(", ");
    }
    pantalla_borrar_cr();
    pantalla_borrar_cr();

    pantalla_escribir(".\nSeleccion actual: `");
    pantalla_escribir(teclado_mapa_actual());
    pantalla_escribir("'.\n");
}

static inline void cambiar_mapa(const char *nombre)
{
    if (!teclado_cambiar_mapa_actual(nombre)) {
        pantalla_escribir("Error: no existe el mapa `");
        pantalla_escribir(nombre);
        pantalla_escribir("'.\n");
    }
}

void comando_teclado(void)
{
    const char *arg = lineacomando_arg();

    if (arg)
        cambiar_mapa(arg);
    else
        listar_mapas();
}
