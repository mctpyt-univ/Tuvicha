/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "tuvicha.h"
#include "../pantalla.h"


const char comando_tuvicha_descripcion[] =
    "mostrar informacion acerca de Tuvicha";

const char comando_tuvicha_args[] = "";


void comando_tuvicha(void)
{
    pantalla_escribir(
"Estas usando el sistema operativo Tuvicha.\n\n"
"Tuvicha es software libre, por supuesto. Esto quiere decir que respeta "
"tus\nderechos de usarlo, adaptarlo y compartirlo libremente. Esta bajo la "
"licencia\nGPLv3, cuyo texto completo se encuentra disponible, en ingles, "
"en el archivo\n`documentacion/licencia.txt' del arbol de codigo fuente de "
"Tuvicha.\n\n"
"Podes descargar el codigo fuente desde este sitio:\n"
"    http://gitorious.org/mstreet_fceia/tuvicha\n\n"
"El autor de Tuvicha es Mariano Street. Podes contactarlo escribiendole un\n"
"correo a `mstreet@kde.org.ar'.\n"
                      );
}
