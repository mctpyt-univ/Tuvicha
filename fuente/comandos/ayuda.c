/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "ayuda.h"
#include "../lineacomando.h"
#include "../pantalla.h"
#include "../utilidades/comun.h"
#include "../utilidades/cadena.h"


const char comando_ayuda_descripcion[] =
    "listar los comandos disponibles y las teclas especiales";

const char comando_ayuda_args[] = "[<comando>]";

static const struct {
    /* Hay que acordarse de actualizar estas longitudes cuando cambien los
       contenidos. */
    char tecla[4];
    char descripcion[38];
} teclas_especiales[] = {
    { "F1",  "escribir ayuda en la linea de comando" },
    { "F2",  "subir en el historial de comandos"     },
    { "F3",  "bajar en el historial de comandos"     },
    { "Tab", "autocompletar nombre de comando"       }
};


static inline void describir_comando(const Comando *comandos,
                                     const char    *comando)
{
    uint8 i;

    for (i = 0; i < lineacomando_cantidad_comandos(); ++i)
        if (cadena_iguales(comandos[i].nombre, comando)) {
            pantalla_escribir("Comando ");
            pantalla_resaltar();
            pantalla_escribir(comando);
            pantalla_resaltar();
            pantalla_escribir(":\n    Descripcion: ");
            pantalla_escribir(comandos[i].descripcion);
            pantalla_escribir("\n    Uso: ");
            pantalla_escribir(comando);
            pantalla_escribir_cr(' ');
            pantalla_escribir(comandos[i].args);
            pantalla_escribir_cr('\n');
            return;
        }
    pantalla_escribir("Error: no existe el comando `");
    pantalla_escribir(comando);
    pantalla_escribir("'.\n");
}

static inline void listar_comandos(const Comando *comandos)
{
    uint8 i;

    pantalla_escribir("Comandos disponibles:\n");
    for (i = 0; i < lineacomando_cantidad_comandos(); ++i) {
        pantalla_escribir("    ");
        pantalla_resaltar();
        pantalla_escribir(comandos[i].nombre);
        pantalla_resaltar();
        pantalla_escribir(": ");
        pantalla_escribir(comandos[i].descripcion);
        pantalla_escribir_cr('\n');
    }
    pantalla_escribir("Escribi `ayuda <comando>' para obtener mas ayuda "
                      "sobre un comando en particular."
                      "\nTeclas especiales:\n");
    for (i = 0; i < LONGITUD(teclas_especiales); ++i) {
        pantalla_escribir("    ");
        pantalla_resaltar();
        pantalla_escribir(teclas_especiales[i].tecla);
        pantalla_resaltar();
        pantalla_escribir(": ");
        pantalla_escribir(teclas_especiales[i].descripcion);
        pantalla_escribir_cr('\n');
    }
}

void comando_ayuda(void)
{
    const char    *arg      = lineacomando_arg();
    const Comando *comandos = lineacomando_comandos();

    if (arg)
        describir_comando(comandos, arg);
    else
        listar_comandos(comandos);
}
