/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "puerto.h"
#include "utilidades/asegurar.h"


void puerto_leer_bloque_2(const uint16       puerto,
                          uint16            *bloque,
                          const unsigned int tamanio)
{
    asegurar(bloque != NULL);
    asegurar(tamanio != 0);

    __asm__ __volatile__("repne insw"
                         : "=m" (bloque)
                         : "c" (tamanio), "d" (puerto), "D" (bloque));
}
