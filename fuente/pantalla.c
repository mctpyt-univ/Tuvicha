/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "pantalla.h"
#include "cga.h"
#include "utilidades/asegurar.h"
#include "utilidades/cadena.h"


/**
 * Atributo en uso, a aplicar en las escrituras.
 */
static uint8 atributo_activo;

/**
 * Atributo en desuso, que pasará a estar en uso al llamar a
 * `pantalla_resaltar'.
 */
static uint8 atributo_inactivo;


static inline uint8 generar_atributo(const PantallaColor color_texto,
                                     const PantallaColor color_fondo)
{
    return color_texto | color_fondo << 4;
}

void pantalla_borrar_cr(void)
{
    cga_borrar(atributo_activo);
}

void pantalla_desactivar_cursor(void)
{
    cga_desactivar_cursor();
}

void pantalla_escribir(const char *cadena)
{
    asegurar(cadena != NULL);

    for (; *cadena; ++cadena)
        pantalla_escribir_cr(*cadena);
}

void pantalla_escribir_cr(const char caracter)
{
    if (caracter == '\a')
        pantalla_resaltar();
    else
        cga_escribir(caracter, atributo_activo);
}

void pantalla_escribir_numero(unsigned int numero,
                              const uint8  base,
                              const uint8  ancho_min)
{
    char cadena[CADENA_DESDE_NUMERO_LONGITUD];

    pantalla_escribir(cadena_desde_numero(cadena, numero, base, ancho_min));
}

void pantalla_escribir_numero_hexa(unsigned int numero,
                                   const uint8  ancho_min)
{
    char cadena[CADENA_DESDE_NUMERO_HEXA_LONGITUD];

    pantalla_escribir(cadena_desde_numero_hexa(cadena, numero, ancho_min));
}

void pantalla_escribir_relleno(const char  *cadena,
                               unsigned int ancho_min)
{
    asegurar(cadena != NULL);

    for (; *cadena; ++cadena, --ancho_min)
        pantalla_escribir_cr(*cadena);
    while (ancho_min--)
        pantalla_escribir_cr(' ');
}

void pantalla_escribir_titulo(const char         *cadena,
                              const PantallaColor color_texto,
                              const PantallaColor color_fondo)
{
    const uint8 atributo_activo_original = atributo_activo;

    asegurar(cadena != NULL);

    atributo_activo = generar_atributo(color_texto, color_fondo);

    cga_llenar_fila(atributo_activo);
    pantalla_escribir("  ");
    pantalla_escribir(cadena);
    cga_llenar_fila(atributo_activo);
    cga_llenar_fila(atributo_activo);

    atributo_activo = atributo_activo_original;
    cga_establecer_cursor_minimo();
}

void pantalla_inicializar(const PantallaColor color_texto_normal,
                          const PantallaColor color_fondo_normal,
                          const PantallaColor color_texto_resaltado,
                          const PantallaColor color_fondo_resaltado)
{
    atributo_activo   = generar_atributo(color_texto_normal,
                                         color_fondo_normal);
    atributo_inactivo = generar_atributo(color_texto_resaltado,
                                         color_fondo_resaltado);
    pantalla_limpiar();
}

void pantalla_limpiar(void)
{
    cga_limpiar(atributo_activo);
}

void pantalla_resaltar(void)
{
    uint8 atributo;

    /* Intercambiamos atributos. */
    atributo          = atributo_inactivo;
    atributo_inactivo = atributo_activo;
    atributo_activo   = atributo;
}

void pantalla_sincronizar_cursor(void)
{
    cga_sincronizar_cursor();
}
