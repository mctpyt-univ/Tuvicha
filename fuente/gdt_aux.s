;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2013 Mariano Street.                                        ;;
;; Este archivo es parte de Tuvicha.                                       ;;
;;                                                                         ;;
;; Tuvicha is free software: you can redistribute it and/or modify it      ;;
;; under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or (at your  ;;
;; option) any later version.                                              ;;
;;                                                                         ;;
;; Tuvicha is distributed in the hope that it will be useful, but WITHOUT  ;;
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   ;;
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   ;;
;; for more details.                                                       ;;
;;                                                                         ;;
;; You should have received a copy of the GNU General Public License       ;;
;; along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


        extern  gdt_puntero

SELECTOR_DE_CODIGO:     equ 1 * 8
SELECTOR_DE_DATOS:      equ 2 * 8


        section .text

        global  gdt_instalar
gdt_instalar:
        lgdt    [gdt_puntero]
        mov     ax, SELECTOR_DE_DATOS
        mov     ds, ax
        mov     es, ax
        mov     fs, ax
        mov     gs, ax
        mov     ss, ax
        jmp     SELECTOR_DE_CODIGO:.salto
.salto:
        ret
