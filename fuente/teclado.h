/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Módulo para manejar el teclado AT.                                      *
 ***************************************************************************/
#ifndef TUVICHA__TECLADO__H
#define TUVICHA__TECLADO__H


#include "utilidades/comun.h"


/**
 * Puerto del controlador de teclado AT: el Intel 8042. Al ser leído,
 * da acceso al registro de estado; al ser escrito, al registro de comando.
 */
#define TECLADO_CONTROLADOR_PUERTO    0x64

typedef enum {
    TECLADO_LUZ_DESPL = 1,
    TECLADO_LUZ_NUM   = 2,
    TECLADO_LUZ_MAYUS = 4
} TecladoLuz;

/**
 * Prender o apagar luces del teclado.
 */
bool         teclado_cambiar_luces(TecladoLuz luces);

/**
 * Cambiar el mapa de teclado actual.
 *
 * @param nombre  cadena con el nombre del mapa de teclado a establecer.
 */
bool         teclado_cambiar_mapa_actual(const char *nombre);

/**
 * Obtener la cantidad de mapas de teclado disponibles.
 */
unsigned int teclado_cantidad_mapas(void);

/**
 * Obtener e interpretar la última tecla presionada en el teclado.
 */
void         teclado_interpretar_tecla(void);

/**
 * Obtener el nombre del mapa de teclado actual.
 */
const char  *teclado_mapa_actual(void);

/**
 * Obtener el nombre de un mapa de teclado.
 */
const char  *teclado_mapa(const unsigned int indice);


#endif
