/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Módulo para gestionar los manejadores de interrupción en modo protegido *
 * de x86. Se encarga de configurar la IDT (“Interrupt Descriptor Table”)  *
 * y los PIC (“Programmable Interrupt Controllers”).                       *
 ***************************************************************************/
#ifndef TUVICHA__IDT__H
#define TUVICHA__IDT__H


#include "utilidades/comun.h"


typedef void (*IDTManejador)(void);

/**
 * Instalar en la IDT un manejador de interrupción.
 *
 * @sa idt_instalar_manejador_irq
 */
void idt_instalar_manejador(const uint8  indice,
                            IDTManejador manejador);

/**
 * Instalar en la IDT un manejador de interrupción correspondiente a una IRQ,
 * y habilitar tal IRQ en los PIC. Los cambios en los PIC no se aplicarán
 * hasta la próxima llamada a `idt_inicializar'.
 *
 * @sa idt_instalar_manejador
 */
void idt_instalar_manejador_irq(const uint8  irq,
                                IDTManejador manejador);

/**
 * Inicializar la IDT y los PIC.
 */
void idt_inicializar(void);


#endif
