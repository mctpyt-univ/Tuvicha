/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Módulo para manejar la pantalla abstrayéndose del tipo de la misma.     *
 ***************************************************************************/
#ifndef TUVICHA__PANTALLA__H
#define TUVICHA__PANTALLA__H


#include "utilidades/comun.h"


typedef enum {
    PANTALLA_COLOR_NEGRO = 0,
    PANTALLA_COLOR_AZUL,
    PANTALLA_COLOR_VERDE_OSCURO,
    PANTALLA_COLOR_CIAN_OSCURO,
    PANTALLA_COLOR_ROJO,
    PANTALLA_COLOR_MAGENTA_OSCURO,
    PANTALLA_COLOR_MARRON_OSCURO,
    PANTALLA_COLOR_GRIS_CLARO,
    PANTALLA_COLOR_GRIS_OSCURO,
    PANTALLA_COLOR_CELESTE,
    PANTALLA_COLOR_VERDE_CLARO,
    PANTALLA_COLOR_CIAN_CLARO,
    PANTALLA_COLOR_ROSA,
    PANTALLA_COLOR_MAGENTA_CLARO,
    PANTALLA_COLOR_MARRON_CLARO,
    PANTALLA_COLOR_BLANCO
} PantallaColor;


/**
 * Borrar el último caracter escrito en la pantalla y actualizar la posición.
 */
void pantalla_borrar_cr(void);

/**
 * Ocultar el cursor de la pantalla.
 */
void pantalla_desactivar_cursor(void);

/**
 * Escribir una cadena de caracteres en la pantalla, empezando en la posición
 * actual del cursor, y actualizar la posición.
 *
 * @param cadena  cadena a escribir.
 *
 * @sa pantalla_escribir_cr, pantalla_escribir_relleno
 */
void pantalla_escribir(const char *cadena);

/**
 * Escribir un caracter en la pantalla, en la posición actual del cursor, y
 * actualizar la posición. Si se manda a escribir el caracter de alerta (\a),
 * se produce en cambio una llamada a la función `pantalla_resaltar'.
 *
 * @param caracter  caracter a escribir.
 */
void pantalla_escribir_cr(const char caracter);

/**
 * Escribir un número entero en la pantalla con notación en la base numérica
 * indicada. No se incluyen afijos que identifiquen la base.
 *
 * @param numero     número a representar en pantalla.
 * @param base       base numérica con la cual representar, entre 2 y 10.
 * @param ancho_min  cantidad mínima de caracteres a escribir (se rellena con
 *                   espacios de ser necesario).
 *
 * @sa pantalla_escribir_numero_hexa
 */
void pantalla_escribir_numero(unsigned int numero,
                              const uint8  base,
                              const uint8  ancho_min);

/**
 * Escribir un número entero en la pantalla con notación hexadecimal,
 * empezando en la posición actual del cursor, y actualizar la posición.
 *
 * @param numero     número a representar en pantalla.
 * @param ancho_min  cantidad mínima de caracteres a escribir (se rellena con
 *                   espacios de ser necesario).
 *
 * @sa pantalla_escribir_numero
 */
void pantalla_escribir_numero_hexa(unsigned int numero,
                                   const uint8  ancho_min);

/**
 * Escribir una cadena de caracteres en la pantalla con un ancho mínimo,
 * empezando en la posición actual del cursor, y actualizar la posición.
 * Si la longitud de la cadena es menor que el ancho mínimo, se rellena
 * al final con espacios.
 *
 * @param cadena  cadena a imprimir.
 * @param ancho_min  cantidad mínima de caracteres a escribir (se rellena con
 *                   espacios de ser necesario).
 *
 * @sa pantalla_escribir_cr, pantalla_escribir
 */
void pantalla_escribir_relleno(const char  *cadena,
                               unsigned int ancho_min);

/**
 * Escribir un título de 3 líneas en la parte superior de la pantalla.
 *
 * Nota: la función modifica la posición del cursor y no la restaura.
 */
void pantalla_escribir_titulo(const char         *cadena,
                              const PantallaColor color_texto,
                              const PantallaColor color_fondo);

/**
 * Detectar si la pantalla es monocromática o a color, establecer los colores
 * iniciales y limpiar la pantalla. Esta función debe ser llamada antes que
 * cualquier otra de este módulo.
 */
void pantalla_inicializar(const PantallaColor color_texto_normal,
                          const PantallaColor color_fondo_normal,
                          const PantallaColor color_texto_resaltado,
                          const PantallaColor color_fondo_resaltado);

/**
 * Limpiar la pantalla; el título se mantiene inalterado en caso de haberlo.
 */
void pantalla_limpiar();

/**
 * Activar o desactivar el resalto para las próximas escrituras.
 */
void pantalla_resaltar(void);

/**
 * Sincronizar la posición interna del cursor con la mostrada en pantalla.
 */
void pantalla_sincronizar_cursor(void);


#endif
