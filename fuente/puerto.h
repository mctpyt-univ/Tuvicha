/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Módulo para comunicarse con el hardware por medio de los puertos de     *
 * entrada y salida de x86.                                                *
 ***************************************************************************/
#ifndef TUVICHA__PUERTO__H
#define TUVICHA__PUERTO__H


#include "utilidades/comun.h"


/**
 * Escribir un byte en un puerto.
 *
 * @param puerto  número de puerto al cual escribir.
 * @param dato    byte a escribir en el puerto.
 */
inline void puerto_escribir(const uint16 puerto,
                            const uint8  dato)
{
    __asm__ __volatile__("out %%al, %%dx" : : "a" (dato), "d" (puerto));
}

/**
 * Leer un byte de un puerto.
 *
 * @param puerto  número de puerto del cual leer.
 *
 * @return byte leído del puerto.
 */
inline uint8 puerto_leer(const uint16 puerto)
{
    uint8 resultado;

    __asm__ __volatile__("in %%dx, %%al" : "=a" (resultado) : "d" (puerto));
    return resultado;
}

/**
 * Leer repetidamente palabras (2 bytes) de un puerto y almacenarlas en un
 * bloque en RAM.
 *
 * @param puerto   número de puerto del cual leer.
 * @param bloque   dirección del bloque de RAM donde almacenar los datos.
 * @param tamanio  cantidad de palabras a leer.
 */
void puerto_leer_bloque_2(const uint16       puerto,
                          uint16            *bloque,
                          const unsigned int tamanio);


#endif
