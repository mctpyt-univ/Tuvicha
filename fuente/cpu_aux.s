;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2013 Mariano Street.                                        ;;
;; Este archivo es parte de Tuvicha.                                       ;;
;;                                                                         ;;
;; Tuvicha is free software: you can redistribute it and/or modify it      ;;
;; under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or (at your  ;;
;; option) any later version.                                              ;;
;;                                                                         ;;
;; Tuvicha is distributed in the hope that it will be useful, but WITHOUT  ;;
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   ;;
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   ;;
;; for more details.                                                       ;;
;;                                                                         ;;
;; You should have received a copy of the GNU General Public License       ;;
;; along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


        section .text

        global  _cpu_id_disponible
_cpu_id_disponible:
        ;; Cambiamos bit en EFLAGS.
        pushfd
        pop     eax
        mov     ecx, eax
        xor     eax, 0x200000
        push    eax
        popfd
        ;; Vemos si el cambio surtió efecto.
        pushfd
        pop     eax
        xor     eax, ecx
        shr     eax, 21
        and     eax, 1
        ;; Revertimos EFLAGS y terminamos.
        push    ecx
        popfd
        ret
