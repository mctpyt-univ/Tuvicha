/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "comandos.h"
#include "cpu.h"
#include "disco.h"
#include "gdt.h"
#include "idt.h"
#include "interrupcion.h"
#include "lector.h"
#include "lineacomando.h"
#include "ma.h"
#include "pantalla.h"
#include "utilidades/comun.h"


/* Esta función no debe retornar. */
void inicio(int32  numero_magico,
            int32 *info_arranque)
{
    gdt_instalar();

    pantalla_inicializar(PANTALLA_COLOR_GRIS_CLARO, PANTALLA_COLOR_NEGRO,
                         PANTALLA_COLOR_BLANCO,     PANTALLA_COLOR_NEGRO);
    pantalla_escribir_titulo("Tuvicha te da la bienvenida",
                             PANTALLA_COLOR_BLANCO, PANTALLA_COLOR_AZUL);

    /* Revisamos si el núcleo fue cargado siguiendo la Especificación de
       Multiarranque. */
    if (numero_magico == MA_NUMERO_MAGICO)
        ma_inicializar(info_arranque);
    else
        pantalla_escribir("Advertencia: el nucleo no se cargo siguiendo la "
                          "Especificacion de Multiarranque.");

    comandos_instalar();
    pantalla_escribir("Sentite comodo jugando con los comandos. Podes empezar "
                      "con el comando `");
    pantalla_escribir(lineacomando_nombre_comando_ayuda());
    pantalla_escribir("'.\n");
    lector_mostrar_prompt();

    disco_inicializar();

    idt_instalar_manejador_irq(1, &interrupcion_teclado);
    idt_inicializar();

    /* Dejamos a la CPU descansando mientras no haya interrupciones. Cuando
       salte una interrupción, se la atenderá para luego volver a
       descansar. */
    cpu_dormir();
}
