/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Módulo para gestionar la segmentación de memoria en modo protegido de   *
 * x86. Se encarga de instalar la GDT (“Global Descriptor Table”).         *
 ***************************************************************************/
#ifndef TUVICHA__GDT__H
#define TUVICHA__GDT__H


#define GDT_SELECTOR_DE_CODIGO    (1 * 8)
#define GDT_SELECTOR_DE_DATOS     (2 * 8)


void gdt_instalar(void);


#endif
