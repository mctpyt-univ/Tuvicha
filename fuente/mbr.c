/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "mbr.h"
#include "disco.h"
#include "utilidades/asegurar.h"


#define INICIO_PARTICIONES    0x1BE
#define FIRMA                 0xAA55

typedef struct {
    uint8        arrancable;
    uint8        cabezal_inicial;
    unsigned int sector_inicial : 6;
    unsigned int cilindro_inicial_alto : 2;
    uint8        cilindro_inicial_bajo;
    uint8        id_sistema;
    uint8        cabezal_final;
    unsigned int sector_final : 6;
    unsigned int cilindro_final_alto : 2;
    uint8        cilindro_final_bajo;
    uint32       lba_inicial;
    uint32       cantidad_sectores;
} __attribute__((packed)) ParticionInterna;

static uint8 mbr[512];

/**
 * ¿Hay un MBR cargado en RAM?
 */
static bool  mbr_cargado;

/**
 * Número del disco al que corresponde el MBR cargado.
 */
static Disco disco_del_mbr;

/**
 * Número de la partición seleccionada actualmente.
 */
static uint8 particion_actual;


uint8 mbr_actual(void)
{
    return particion_actual;
}

bool mbr_cambiar_actual(const uint8 indice)
{
    if (indice > MBR_CANTIDAD_PARTICIONES)
        return false;

    if (indice != 0)
        if (!mbr_cargar(false))
            return false;

    particion_actual = indice;
    return true;
}

bool mbr_cargar(const bool forzar)
{
    const Disco disco = disco_actual();

    /* Leemos el primer sector del disco solo en caso de que no lo tengamos
       ya leído y cargado en RAM, o de que se fuerce a cargar el MBR. */
    if (forzar || !mbr_cargado || disco_del_mbr != disco) {
        /* Establecer estas variables de control antes de leer efectivamente
           del disco reduce el problema de la no reentrancia en algunos
           casos. */
        mbr_cargado   = true;
        disco_del_mbr = disco;
        if (disco_leer(0, 1, (uint16 *) mbr))
            return true;
        else {
            mbr_cargado = false;
            return false;
        }
    }
    return true;
}

bool mbr_controlar_firma(void)
{
    asegurar(mbr_cargado);

    return ((uint16 *) mbr)[255] == FIRMA;
}

MBRParticion mbr_particion(const uint8 indice)
{
    ParticionInterna interna;
    MBRParticion     externa;

    asegurar(mbr_cargado);
    asegurar(indice != 0);
    asegurar(indice <= 4);

    interna = ((ParticionInterna *) (mbr + INICIO_PARTICIONES))[indice - 1];

    externa.arrancable        = interna.arrancable & 0x80;
    externa.cabezal_inicial   = interna.cabezal_inicial;
    externa.sector_inicial    = interna.sector_inicial;
    externa.cilindro_inicial  = interna.cilindro_inicial_alto << 8 |
                                interna.cilindro_inicial_bajo;
    externa.id_sistema        = interna.id_sistema;
    externa.cabezal_final     = interna.cabezal_final;
    externa.sector_final      = interna.sector_final;
    externa.cilindro_final    = interna.cilindro_final_alto << 8 |
                                interna.cilindro_final_bajo;
    externa.lba_inicial       = interna.lba_inicial;
    externa.cantidad_sectores = interna.cantidad_sectores;

    return externa;
}

const char *mbr_tipo_particion(const MBRParticion particion)
{
    /* Dado que son pocos los identificadores reconocidos, se compara
       linealmente con todos ellos, y en orden de presunta popularidad. En
       caso de agregar cadenas para todos o la mayoría de los
       identificadores restantes, convendrá cambiar esto por una indexación
       en un arreglo de cadenas. */
    switch (particion.id_sistema) {
        case MBR_TIPO_PARTICION_INEXISTENTE:
            return "inexistente";
        case MBR_TIPO_PARTICION_EXTENDIDA:
            return "Extendida";
        case MBR_TIPO_PARTICION_EXTENDIDA_LBA:
            return "Extendida (LBA)";
        case MBR_TIPO_PARTICION_FAT32:
            return "FAT32";
        case MBR_TIPO_PARTICION_FAT32_LBA:
            return "FAT32 (LBA)";
        case MBR_TIPO_PARTICION_GNULINUX:
            return "GNU/Linux";
        case MBR_TIPO_PARTICION_BSD:
            return "BSD";
        case MBR_TIPO_PARTICION_FAT16:
            return "FAT16";
        case MBR_TIPO_PARTICION_FAT16_CHICO:
            return "FAT16 (chico)";
        case MBR_TIPO_PARTICION_FAT12:
            return "FAT12";
        default:
            return "desconocido";
    }
}
