/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "disco.h"
#include "puerto.h"
#include "utilidades/asegurar.h"
#include "utilidades/cadena.h"


#define ATA_PRIMARIO_PUERTO_BASE      0x1F0
#define ATA_SECUNDARIO_PUERTO_BASE    0x170

#define COMANDO_LEER                  0x20
#define COMANDO_IDENTIFICAR           0xEC

#define ESTADO_ERR                    0x01
#define ESTADO_DRQ                    0x08
#define ESTADO_BSY                    0x40

#define MODO_CHS                      0xA0
#define MODO_LBA28                    0xE0


typedef enum {
    IDENT_SERIE    = 10,
    IDENT_FIRMWARE = 23,
    IDENT_MODELO   = 27,
    IDENT_SECTORES = 60
} IdentCampo;

/**
 * Disco actual.
 */
static Disco     actual;

/**
 * Puerto base correspondiente al canal del disco actual. Si es 0, significa
 * que no hay un canal seleccionado (lo cual ocurre cuando el canal
 * predeterminado no existe).
 */
static uint16    puerto_base;

/**
 * Indicadores de la presencia de los discos maestro y esclavo de los canales
 * ATA primario y secundario.
 */
static DiscoInfo discos[4];


#define GUARDAR_CADENA(disco,bufer,campo_minus,campo_mayus) \
    guardar_cadena(discos[disco]. campo_minus,              \
                   sizeof discos[disco]. campo_minus,       \
                   bufer, IDENT_##campo_mayus)

static void guardar_cadena(char              *destino,
                           const unsigned int tamanio,
                           const uint16      *bufer,
                           const IdentCampo   campo)
{
    unsigned int i;
    char         cadena[tamanio];

    if (bufer[campo] == 0)
        return;

    /* Guardamos la cadena en el búfer temporal. */
    for (i = 0; i < (tamanio - 1) / 2; i++) {
        cadena[2 * i]     = bufer[campo + i] >> 8;
        cadena[2 * i + 1] = bufer[campo + i] & 0xFF;
    }
    cadena[2 * i] = '\0';

    /* Conservamos la cadena, deshaciéndonos de los espacios al principio y
       al final. */
    cadena_recortar(destino, cadena);
}

static void detectar_disco(uint16      *bufer,
                           const uint8  disco)
{
    const uint16 puerto_base = disco & 2 ? ATA_SECUNDARIO_PUERTO_BASE :
                                           ATA_PRIMARIO_PUERTO_BASE;
    /* `modo' indica tanto el modo de direccionamiento como el disco a
       seleccionar en el canal. */
    const uint8  modo        = MODO_CHS | (disco & 1) << 4;

    puerto_escribir(puerto_base + 6, modo);
    puerto_escribir(puerto_base + 2, 0);
    puerto_escribir(puerto_base + 3, 0);
    puerto_escribir(puerto_base + 4, 0);
    puerto_escribir(puerto_base + 5, 0);
    puerto_escribir(puerto_base + 7, COMANDO_IDENTIFICAR);

    if (puerto_leer(puerto_base + 7) == 0)
        return;

    for (;;) {
        const uint8 estado = puerto_leer(puerto_base + 7);
        if (estado & ESTADO_BSY)
            break;
        if (estado & ESTADO_ERR)
            return;
    }

    for (;;) {
        const uint8 estado = puerto_leer(puerto_base + 7);
        if (estado & ESTADO_DRQ)
            break;
        if (estado & ESTADO_ERR)
            return;
    }

    puerto_leer_bloque_2(puerto_base + 0, bufer, 256);

    discos[disco].presente = true;
    discos[disco].sectores = bufer[IDENT_SECTORES] |
                             ((uint32) bufer[IDENT_SECTORES + 1]) << 16;
    GUARDAR_CADENA(disco, bufer, serie,    SERIE);
    GUARDAR_CADENA(disco, bufer, firmware, FIRMWARE);
    GUARDAR_CADENA(disco, bufer, modelo,   MODELO);
}

Disco disco_actual(void)
{
    return actual;
}

bool disco_cambiar_actual(const Disco disco)
{
    /* Solo se puede manejar hasta 4 discos, y solo si su respectivo canal
       ATA está presente. */
    if (disco >= 4 || !discos[disco].presente)
        return false;

    actual      = disco;
    puerto_base = disco & 2 ? ATA_SECUNDARIO_PUERTO_BASE :
                              ATA_PRIMARIO_PUERTO_BASE;
    return true;
}

const DiscoInfo *disco_info(const Disco disco)
{
    if (disco >= 4 || !discos[disco].presente)
        return 0;

    return discos + disco;
}

void disco_inicializar(void)
{
    uint16 bufer[256];

    /* Detectar canales ATA. */
    puerto_escribir(ATA_PRIMARIO_PUERTO_BASE   + 3, 0x12);
    puerto_escribir(ATA_SECUNDARIO_PUERTO_BASE + 3, 0x14);
    if (puerto_leer(ATA_PRIMARIO_PUERTO_BASE + 3) == 0x12) {
        detectar_disco(bufer, 0);
        detectar_disco(bufer, 1);
    }
    if (puerto_leer(ATA_SECUNDARIO_PUERTO_BASE + 3) == 0x14) {
        detectar_disco(bufer, 2);
        detectar_disco(bufer, 3);
    }

    /* Establecer disco predeterminado (en caso de que sea accesible). */
    disco_cambiar_actual(0);
}

bool disco_leer(const uint32 lba,
                uint16       cantidad_sectores,
                uint16      *bloque)
{
    /* `modo' indica tanto el modo de direccionamiento como el disco a
       seleccionar en el canal. */
    const uint8  modo = MODO_LBA28 | (actual & 1) << 4;
    unsigned int i;

    asegurar(lba < 0x0FFFFFFF);
    asegurar(cantidad_sectores != 0);
    asegurar(bloque != NULL);

    if (!puerto_base)
        return false;

    /* Mandamos el comando de lectura al disco. */
    puerto_escribir(puerto_base + 6, modo | lba >> 24);
    puerto_escribir(puerto_base + 2, cantidad_sectores);
    puerto_escribir(puerto_base + 3, lba       & 0xFF);
    puerto_escribir(puerto_base + 4, lba >>  8 & 0xFF);
    puerto_escribir(puerto_base + 5, lba >> 16 & 0xFF);
    puerto_escribir(puerto_base + 7, COMANDO_LEER);

    /* Esperamos a que el búfer esté listo. */
    for (i = 0; !(puerto_leer(puerto_base + 7) & ESTADO_DRQ); ++i)
        /* Si el disco no va a responder... en algún momento nos cansamos de
           esperar. El límite elegido conlleva una demora de unos pocos
           segundos. */
        if (i == 0xFFFFF)
            return false;

    /* Guardamos los datos en la RAM. */
    do
        /* Leemos de a un sector por iteración. */
        puerto_leer_bloque_2(puerto_base + 0, bloque, 512 / 2);
    while (--cantidad_sectores != 0);
    return true;
}
