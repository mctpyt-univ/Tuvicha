/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Módulo para manejar la información brindada por el arrancador según la  *
 * Especificación de Multiarranque.                                        *
 ***************************************************************************/
#ifndef TUVICHA__MA__H
#define TUVICHA__MA__H


#include "utilidades/comun.h"


/**
 * Número mágico que el arrancador debe proveer al núcleo para que este sepa
 * que fue cargado siguiendo la Especificación de Multiarranque.
 */
#define MA_NUMERO_MAGICO    0x2BADB002

typedef struct {
    uint8 particion_3;
    uint8 particion_2;
    uint8 particion_1;
    uint8 disco;
} __attribute__((packed)) MADispositivo;


/**
 * Obtener las banderas pasadas por el arrancador.
 */
uint32      ma_banderas(void);

/**
 * Obtener, en caso de estar disponible, la cantidad de módulos cargados por
 * el arrancador.
 */
bool        ma_cantidad_modulos(uint32 *cantidad_modulos);

/**
 * Obtener, en caso de estar disponible, el dispositivo desde el cual se
 * arrancó el núcleo.
 */
bool        ma_dispositivo(MADispositivo *dispositivo);

/**
 * Controlar si se registró información brindada por el arrancador; en otras
 * palabras, si se llamó a `ma_inicializar'.
 */
bool        ma_inicializado(void);

/**
 * Registrar la información brindada por el arrancador.
 */
void        ma_inicializar(const int32 *info_nueva);

/**
 * Obtener, en caso de estar disponible, la línea de comando mediante la
 * cual se arrancó el núcleo.
 */
const char *ma_linea_comando(void);

/**
 * Obtener, en caso de estar disponibles, las cantidades de memoria baja y
 * alta registradas por el arrancador.
 */
bool        ma_memoria(uint32 *memoria_baja,
                       uint32 *memoria_alta);

/**
 * Obtener, en caso de estar disponible, el nombre del arrancador del núcleo.
 */
const char *ma_nombre_arrancador(void);


#endif
