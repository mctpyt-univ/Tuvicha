/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Controlador de video CGA en modo texto.                                 *
 ***************************************************************************/
#ifndef TUVICHA__CGA__H
#define TUVICHA__CGA__H


#include "utilidades/comun.h"


/**
 * Borrar el último caracter escrito en la pantalla CGA y actualizar la
 * posición.
 */
void cga_borrar(const uint8 atributo);

void cga_desactivar_cursor(void);

/**
 * Escribir un caracter en la pantalla CGA con el atributo indicado, en la
 * posición actual del cursor, y actualizar la posición.
 */
void cga_escribir(const char  caracter,
                  const uint8 atributo);

/**
 * Establecer la posición actual del cursor como posición mínima. Esto quiere
 * decir que el contenido de la pantalla CGA desde el principio hasta tal
 * posición no será alterado por escrituras o desplazamientos.
 */
void cga_establecer_cursor_minimo(void);

/**
 * Llenar lo que queda de la fila actual en la pantalla CGA con el atributo
 * indicado.
 */
void cga_llenar_fila(const uint8 atributo);

/**
 * Limpiar la pantalla CGA.
 */
void cga_limpiar(const uint8 atributo);

/**
 * Sincronizar la posición interna del cursor con la mostrada en la pantalla
 * CGA.
 *
 * Dado que esta función es relativamente lenta, es preferible llamarla solo
 * cuando sea estrictamente necesario: si hay varias cosas para escribir
 * seguidamente en pantalla y solo se requiere interacción del usuario tras
 * haber escrito todas, conviene sincronizar el cursor una única vez, al
 * final.
 */
void cga_sincronizar_cursor(void);


#endif
