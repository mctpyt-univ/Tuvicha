/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "lineacomando.h"
#include "pantalla.h"
#include "utilidades/asegurar.h"
#include "utilidades/cadena.h"


/**
 * Base de datos de comandos: contiene enlaces a las funciones de los
 * distintos comandos y metadatos sobre los mismos.
 */
static Comando     comandos[10];

/**
 * Cantidad de comandos instalados, es decir, cantidad de entradas en uso del
 * vector `comandos'.
 */
static uint8       cantidad_comandos = 0;

/**
 * Nombre del comando a citar como comando de ayuda.
 */
static const char *nombre_comando_ayuda;


char *lineacomando_arg(void)
{
    return cadena_componente(NULL, ' ');
}

uint8 lineacomando_cantidad_comandos(void)
{
    return cantidad_comandos;
}

const Comando *lineacomando_comandos(void)
{
    return comandos;
}

void lineacomando_ejecutar(const char        *linea,
                           const unsigned int longitud)
{
    const char  *comando;
    char         linea_copiada[longitud];
    unsigned int i;

    asegurar(linea != NULL);

    /* Extraemos el nombre del comando. Si la línea está vacía o contiene
       solo espacios, no hacemos nada más. */
    cadena_copiar(linea_copiada, linea);
    if (!(comando = cadena_componente(linea_copiada, ' ')))
        return;

    /* Hacemos una búsqueda lineal para el comando. */
    for (i = 0; i < cantidad_comandos; ++i)
        if (cadena_iguales(comando, comandos[i].nombre)) {
            (*comandos[i].funcion)();
            return;
        }

    pantalla_escribir("Error: no existe el comando `");
    pantalla_escribir(comando);
    pantalla_escribir("'.\nEscribi `");
    pantalla_escribir(nombre_comando_ayuda);
    pantalla_escribir("' para obtener una lista de los comandos "
                      "disponibles.\n");
}

void lineacomando_establecer_comando_ayuda(const char *nombre)
{
    asegurar(nombre != NULL);

    nombre_comando_ayuda = nombre;
}

bool lineacomando_instalar_comando(const char    *nombre,
                                   const char    *descripcion,
                                   const char    *args,
                                   ComandoFuncion funcion)
{
    asegurar(nombre != NULL);
    asegurar(descripcion != NULL);
    asegurar(args != NULL);
    asegurar(funcion != NULL);

    if (cantidad_comandos >= LONGITUD(comandos))
        return 0;

    comandos[cantidad_comandos].nombre      = nombre;
    comandos[cantidad_comandos].descripcion = descripcion;
    comandos[cantidad_comandos].args        = args;
    comandos[cantidad_comandos++].funcion   = funcion;
    return 1;
}

const char *lineacomando_nombre_comando_ayuda(void)
{
    return nombre_comando_ayuda;
}
