;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2013 Mariano Street.                                        ;;
;; Este archivo es parte de Tuvicha.                                       ;;
;;                                                                         ;;
;; Tuvicha is free software: you can redistribute it and/or modify it      ;;
;; under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or (at your  ;;
;; option) any later version.                                              ;;
;;                                                                         ;;
;; Tuvicha is distributed in the hope that it will be useful, but WITHOUT  ;;
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   ;;
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   ;;
;; for more details.                                                       ;;
;;                                                                         ;;
;; You should have received a copy of the GNU General Public License       ;;
;; along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Las funciones de este módulo sirven como manejadores de interrupción de ;;
;; x86. Consisten en abstracciones para funciones de más alto nivel, que   ;;
;; están en C. Dado que las funciones de C no pueden emplearse como        ;;
;; manejadores de interrupción, se necesitan tales abstracciones en        ;;
;; ensamblador. Además, estas funciones se encargan de toda la             ;;
;; comunicación necesaria con los PIC.                                     ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


        extern  teclado_interpretar_tecla


        section .text

        global  interrupcion_teclado
interrupcion_teclado:
        ;; Guardamos el estado anterior del sistema.
        pushf
        push    eax
        push    ecx
        push    edx
        ;; Llamamos a la función de alto nivel.
        call    teclado_interpretar_tecla
        ;; Acusamos recibo de la IRQ al PIC.
        mov     al, 0x20
        out     0x20, al
        ;; Restauramos el estado anterior del sistema.
        pop     edx
        pop     ecx
        pop     eax
        popf
        iret
