/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Módulo para manejar la lectura de líneas de texto por parte del         *
 * usuario. No solo se encarga de mantener búferes internos sino que       *
 * también maneja transparentemente el texto mostrado en pantalla para la  *
 * línea de comando.                                                       *
 ***************************************************************************/
#ifndef TUVICHA__LECTOR__H
#define TUVICHA__LECTOR__H


#include "utilidades/comun.h"


/**
 * Ejecutar el comando correspondiente a la línea de comando actual y, a
 * continuación, mostrar el “prompt”.
 */
void lector_aceptar(void);

/**
 * Autocompletar nombre de comando tomando como prefijo el texto ingresado en
 * la línea de comando. En caso de ambigüedad, se muestra siempre el primer
 * resultado encontrado.
 */
void lector_autocompletar(void);

/**
 * Borrar el último caracter ingresado en la línea de comando.
 */
void lector_borrar_cr(void);

/**
 * Escribir el nombre del comando de ayuda en la línea de comando.
 */
void lector_escribir_ayuda(void);

/**
 * Escribir, si hay espacio libre en el búfer interno, un caracter en la
 * línea de comando.
 */
void lector_escribir_cr(const char caracter);

/**
 * Mostrar por pantalla el “prompt”, es decir, la cadena que indica al
 * usuario que se espera que ingrese texto.
 */
void lector_mostrar_prompt(void);

/**
 * Seleccionar el elemento anterior o el siguiente en el historial.
 *
 * @param bajar  sentido en el cual moverse en el historial: con falso se
 *               sube (se selecciona el elemento anterior) y con verdadero se
 *               baja (se selecciona el elemento siguiente).
 */
void lector_moverse_en_historial(const bool bajar);


#endif
