;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2013 Mariano Street.                                        ;;
;; Este archivo es parte de Tuvicha.                                       ;;
;;                                                                         ;;
;; Tuvicha is free software: you can redistribute it and/or modify it      ;;
;; under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or (at your  ;;
;; option) any later version.                                              ;;
;;                                                                         ;;
;; Tuvicha is distributed in the hope that it will be useful, but WITHOUT  ;;
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   ;;
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   ;;
;; for more details.                                                       ;;
;;                                                                         ;;
;; You should have received a copy of the GNU General Public License       ;;
;; along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Macros auxiliares para mostrar texto por pantalla durante el proceso de ;;
;; arranque. Útiles para depurar.                                          ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;; Escribir en pantalla un caracter, estando en modo real. Se asume que
;;; se llamó previamente a `pantalla_inicializar'.
;;; Parámetros: %1 <- caracter en ASCII (8 bits)
;;;             %2 <- atributos         (8 bits)
;;;             %3 <- posición         (16 bits)
%macro pantalla_escribir_caracter 3
        mov     word [gs:0 + 2 * %3], %1 | %2 << 8
%endmacro

;;; Escribir en pantalla un caracter, estando en modo protegido.
;;; Parámetros: %1 <- caracter en ASCII (8 bits)
;;;             %2 <- atributos         (8 bits)
;;;             %3 <- posición         (16 bits)
%macro pantalla_escribir_caracter_32 3
        mov     word [0xB8000 + 2 * %3], %1 | %2 << 8
%endmacro

;;; Escribir en pantalla un mensaje de error.
;;; Parámetros: %1 <- número de error (entre 0 y 9)
%macro pantalla_escribir_error 1
        pantalla_escribir_caracter 'E',      0x4F, 0
        pantalla_escribir_caracter 'r',      0x4F, 1
        pantalla_escribir_caracter 'r',      0x4F, 2
        pantalla_escribir_caracter 'o',      0x4F, 3
        pantalla_escribir_caracter 'r',      0x4F, 4
        pantalla_escribir_caracter ' ',      0x4F, 5
        pantalla_escribir_caracter '0' + %1, 0x4F, 6
%endmacro

;;; Escribir en pantalla un entero de 32 bits en hexadecimal.
;;; Registros alterados: EAX, EBX, ECX, ESI y EDI.
%macro pantalla_escribir_entero_32 1
        mov     eax, %1
        mov     esi, mapa_hex
        mov     edi, 0xB8000
        mov     ecx, 8
.bucle:
        rol     eax, 4
        mov     bl, al
        and     ebx, 0xF
        mov     bl, [ebx + esi]
        mov     [edi], bl
        mov     byte [edi + 1], 0xF
        add     edi, 2
        loop    .bucle
        jmp     $ + 16
.mapa_hexa:
        db      '0123456789ABCDEF'
%endmacro

;;; Inicializar pantalla. Este código debe ser ejecutado antes de llamar a
;;; cualquiera de las macros anteriores que funcionan en modo real (es decir,
;;; las que no están sufijadas con _32).
;;; Registros alterados: AX y GS.
%macro pantalla_inicializar 0
        mov     ax, 0xB800
        mov     gs, ax
%endmacro
