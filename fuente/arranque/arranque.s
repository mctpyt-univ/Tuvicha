;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2013 Mariano Street.                                        ;;
;; Este archivo es parte de Tuvicha.                                       ;;
;;                                                                         ;;
;; Tuvicha is free software: you can redistribute it and/or modify it      ;;
;; under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or (at your  ;;
;; option) any later version.                                              ;;
;;                                                                         ;;
;; Tuvicha is distributed in the hope that it will be useful, but WITHOUT  ;;
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   ;;
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   ;;
;; for more details.                                                       ;;
;;                                                                         ;;
;; You should have received a copy of the GNU General Public License       ;;
;; along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


%include "gdt.inc"
%include "pantalla.inc"

%define NUCLEO_DIRE    0x1000
%define NUCLEO_SECTORES     0


        [ORG 0x7C00]

;;; Acá empieza la ejecución del programa. Primero opera modo real y después
;;; pasa modo protegido.
arranque:
        [BITS 16]
        pantalla_inicializar
        xor     ax, ax
        mov     ds, ax
        mov     es, ax

.reiniciar_disco:
        ;; Reiniciamos el disco desde el cual arrancamos, para después poder
        ;; leerlo. A la interrupción le pasamos:
        ;;     AH <- operación
        ;;     DL <- identificador del disco
        ;; y devuelve el resultado en AH (0 si no hubo error).
        int     0x13
        or      ah, ah
        jnz     .reiniciar_disco

.leer_disco:
        ;; Leemos del disco los sectores donde está del núcleo. A la interrupción le
        ;; pasamos:
        ;;     AH    <- operación
        ;;     AL    <- cantidad de sectores
        ;;     ES:BX <- dirección donde almacenar los datos
        ;;     CH    <- cilindro del disco desde donde empezar
        ;;     CL    <- sector del disco desde donde empezar
        ;;     DH    <- cabezal del disco desde donde empezar
        ;;     DL    <- identificador del disco
        mov     ax, 0x0200 | NUCLEO_SECTORES
        mov     cx, 0x0002
        mov     bx, NUCLEO_DIRE
        xor     dh, dh
        int     0x13
        or      ah, ah
        jz      .activar_linea_a20
        pantalla_escribir_error 1
        xor     ah, ah
        jmp     .reiniciar_disco

.activar_linea_a20:
        ;; Uno de los varios métodos para activar la línea A20. Dado que nada
        ;; nos garantiza que este vaya a ser compatible con la máquina en
        ;; particular que vayamos a usar, habría que agregar otros.
        in      al, 0x92
        test    al, 2
        jnz     .cargar_modo_protegido
        or      al, 2
        and     al, 0xFE
        out     0x92, al

.cargar_modo_protegido:
        ;; Pasamos a modo protegido de 32 bits, con segmentación plana pero
        ;; sin paginación.
        cli
        lgdt    [gdt]
        mov     eax, cr0
        or      al, 1
        mov     cr0, eax
        ;; Hacemos un salto largo a la siguiente instrucción para alterar CS y
	;; limpiar la tubería de instrucciones de la CPU. CS todavía contiene
	;; una dirección de segmento de modo real, así que debemos cargarle un
        ;; selector de modo protegido con los fines de no tener restricciones
        ;; de direccionamiento y de establecer el tamaño por defecto de
        ;; operadores en 32 bits. Si bien CS tranquilamente se podría modificar
        ;; en el salto que hay más adelante, la tubería debe ser limpiada en
        ;; este preciso momento para asegurarse de que los registros de
	;; segmento que se cargan a continuación lo hagan en modo protegido.
        jmp     dword 0x8:.protegido

        [BITS 32]
.protegido:
        mov     ax, 0x10
        mov     ds, ax
        mov     ss, ax
        mov     esp, 0x9000
        movzx   edx, dl
        push    edx
        jmp     NUCLEO_DIRE


;;; Datos.

gdt:    gdt_empezar gdt, gdt_fin
        ;; Segmento de código, leer/ejecutar, no conforme.
        gdt_descriptor 0xFFFFF, 0, 0b10011010, 0b1100
        ;; Segmento de datos, leer/escribir, expandir hacia abajo.
        gdt_descriptor 0xFFFFF, 0, 0b10010010, 0b1100
gdt_fin:

        times   510 - ($ - $$) db 0
        dw      0xAA55
