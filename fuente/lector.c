/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "lector.h"
#include "lineacomando.h"
#include "pantalla.h"
#include "utilidades/anillo.h"
#include "utilidades/cadena.h"


/**
 * Anillo de búferes donde se almacenan las líneas de texto introducidas por
 * el usuario. No solo la actual sino cierto número de líneas anteriores,
 * para así poder proveer un historial.
 */
static Anillo     buferes;

/**
 * Posición en el historial relativa a la posición actual. 0 indica que no se
 * está navegando el historial.
 */
static uint8      historial;

/**
 * Cadena para indicar al usuario que se espera que ingrese texto.
 */
static const char prompt[] = "\a> \a";


/**
 * Limpiar la línea que se está mostrando actualmente en la pantalla.
 */
static void borrar_caracteres_ingresados(void)
{
    unsigned int i;

    for (i = anillo_ocupado(&buferes, historial); i > 0; --i)
        pantalla_borrar_cr();
}

/**
 * Copiar la entrada actualmente visitada del historial al búfer actual. Hay
 * que procurar hacer esto *antes* de llamar a funciones del anillo que
 * operen sobre el búfer actual sin tener en cuenta el historial.
 */
static void copiar_del_historial(void)
{
    const char *linea_visitada = anillo_cadena(&buferes, historial);

    anillo_girar(&buferes);
    anillo_reemplazar(&buferes, linea_visitada);
    historial = 0;
}

void lector_aceptar(void)
{
    /* Ejecutamos el comando ingresado. */
    pantalla_escribir_cr('\n');
    lineacomando_ejecutar(anillo_cadena(&buferes, historial),
                          ANILLO_CADENA_LONGITUD);
    lector_mostrar_prompt();
    /* Pasamos al siguiente búfer. */
    anillo_girar(&buferes);
    historial = 0;
}

void lector_autocompletar(void)
{
    const Comando *comandos = lineacomando_comandos();
    char           ingresado[ANILLO_CADENA_LONGITUD];
    unsigned int   copiado;
    uint8          i;

    if (historial)
        copiar_del_historial();

    /* Copiamos la línea de comando deshaciéndonos de los espacios en los
       extremos. */
    copiado = cadena_recortar(ingresado, anillo_cadena(&buferes, 0));

    for (i = 0; i < lineacomando_cantidad_comandos(); ++i)
        if (cadena_prefijo(comandos[i].nombre, ingresado)) {
            const unsigned int escrito = anillo_ocupado(&buferes, 0);

            if (copiado == escrito)
                /* Escribimos en pantalla solo la parte que falta del nombre
                   del comando. */
                pantalla_escribir(comandos[i].nombre + escrito);
            else {
                /* Borramos la línea de comando y escribimos todo de nuevo,
                   para deshacernos de los espacios. */
                borrar_caracteres_ingresados();
                pantalla_escribir(comandos[i].nombre);
            }

            pantalla_sincronizar_cursor();
            /* Pendiente: agregar los caracteres faltantes al búfer sin
               reescribir los que ya estén, en caso de que no haya
               espacios. */
            anillo_reemplazar(&buferes, comandos[i].nombre);
            break;
        }
}

void lector_borrar_cr(void)
{
    if (historial)
        copiar_del_historial();

    /* Borramos el último caracter en caso de haberlo. */
    if (anillo_borrar_cr(&buferes)) {
        pantalla_borrar_cr();
        pantalla_sincronizar_cursor();
    }
}

void lector_escribir_ayuda(void)
{
    /* Escribimos el comando de ayuda por el usuario. */
    borrar_caracteres_ingresados();
    anillo_girar(&buferes);
    anillo_reemplazar(&buferes, lineacomando_nombre_comando_ayuda());
    pantalla_escribir(anillo_cadena(&buferes, 0));
    pantalla_sincronizar_cursor();
}

void lector_escribir_cr(const char caracter)
{
    /* Agregamos el caracter indicado. */
    if (anillo_hay_lugar(&buferes, historial)) {
        if (historial)
            copiar_del_historial();
        anillo_agregar_cr(&buferes, caracter);
        pantalla_escribir_cr(caracter);
        pantalla_sincronizar_cursor();
    }
}

void lector_mostrar_prompt(void)
{
    pantalla_escribir(prompt);
    pantalla_sincronizar_cursor();
}

void lector_moverse_en_historial(const bool bajar)
{
    if (bajar && !historial ||
        !bajar && historial >= ANILLO_LONGITUD - 1)
        return;

    borrar_caracteres_ingresados();
    if (bajar)
        --historial;
    else
        ++historial;
    pantalla_escribir(anillo_cadena(&buferes, historial));
    pantalla_sincronizar_cursor();
}
