/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************
 * Módulo para leer datos de discos duros ATA-2.                           *
 ***************************************************************************/
#ifndef TUVICHA__DISCO__H
#define TUVICHA__DISCO__H


#include "utilidades/comun.h"


typedef enum {
    DISCO_PRIMARIO_MAESTRO = 0,
    DISCO_PRIMARIO_ESCLAVO,
    DISCO_SECUNDARIO_MAESTRO,
    DISCO_SECUNDARIO_ESCLAVO
} Disco;

typedef struct {
    /** ¿Está presente el disco? */
    bool   presente;
    /** Cantidad de sectores que tiene el disco. */
    uint32 sectores;
    /** Número de serie. */
    char   serie[21];
    /** Número de revisión del “firmware”. */
    char   firmware[9];
    /** Modelo. */
    char   modelo[41];
} DiscoInfo;

/**
 * Obtener el número del disco ATA seleccionado actualmente.
 *
 * @return número del disco seleccionado.
 */
Disco            disco_actual(void);

/**
 * Cambiar la selección actual de disco ATA.
 *
 * @param disco  número del disco a seleccionar.
 * @return verdadero si se pudo seleccionar el disco, falso si no.
 */
bool             disco_cambiar_actual(const Disco disco);

/**
 * Obtener información sobre un disco ATA, en caso de que el mismo esté
 * esté presente.
 *
 * @param disco  número del disco del cual obtener información.
 * @return puntero a una estructura con información sobre un disco, o nulo si
 *         el disco no está presente.
 */
const DiscoInfo *disco_info(const Disco disco);

/**
 * Detectar los canales y discos ATA presentes.
 */
void             disco_inicializar(void);

/**
 * Leer sectores (de 512 bytes cada uno) del disco seleccionado a partir de
 * una dirección indicada en formato LBA (“Logical Block Address”) de 28
 * bits.
 *
 * @param lba                dirección inicial, no debe ser mayor que
 *                           0x0FFFFFFF (es decir, los 4 bits más
 *                           significativos deben ser 0).
 * @param cantidad_sectores  cantidad de sectores a leer.
 * @param bloque             dirección del bloque de memoria donde almacenar
 *                           los datos leídos.
 * @return verdadero si la lectura tuvo éxito, falso si no.
 */
bool             disco_leer(const uint32 lba,
                            uint16       cantidad_sectores,
                            uint16      *bloque);


#endif
