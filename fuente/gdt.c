/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "gdt.h"
#include "utilidades/comun.h"


#define GDT_DESCRIPTOR_NULO    { 0, 0, 0, 0, 0, 0, 0 }
#define GDT_DESCRIPTOR(limite,base,acceso,banderas) {             \
        (limite)       & 0xFFFF,                                  \
        (base)         & 0xFFFF,                                  \
        (base)   >> 16 & 0xFF,                                    \
        (acceso),                                                 \
        (banderas),                                               \
        (limite) >> 16,                                           \
        (base)   >> 24                                            \
    }

typedef struct {
    uint16 limite0;
    uint16 base0;
    uint8  base1;
    uint8  acceso;
    uint32 banderas : 4;
    uint32 limite1  : 4;
    uint8  base2;
} __attribute__((packed)) GDTDescriptor;

typedef struct {
    uint16 limite;
    uint32 base;
} __attribute__((packed)) GDTPuntero;


static GDTDescriptor gdt[] = {
    GDT_DESCRIPTOR_NULO,
    /* Segmento de código. */
    GDT_DESCRIPTOR(0xFFFFF, 0, 0x9A, 0xC),
    /* Segmento de datos. */
    GDT_DESCRIPTOR(0xFFFFF, 0, 0x92, 0xC)
};

GDTPuntero gdt_puntero = { sizeof gdt - 1, (uint32) &gdt };
