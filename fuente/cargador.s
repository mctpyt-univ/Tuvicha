;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2013 Mariano Street.                                        ;;
;; Este archivo es parte de Tuvicha.                                       ;;
;;                                                                         ;;
;; Tuvicha is free software: you can redistribute it and/or modify it      ;;
;; under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or (at your  ;;
;; option) any later version.                                              ;;
;;                                                                         ;;
;; Tuvicha is distributed in the hope that it will be useful, but WITHOUT  ;;
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   ;;
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   ;;
;; for more details.                                                       ;;
;;                                                                         ;;
;; You should have received a copy of the GNU General Public License       ;;
;; along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


        extern  inicio

MA_ALINEAR_PAGINA:      equ 1 << 0
MA_NUMERO_MAGICO:       equ 0x1BADB002
MA_BANDERAS:            equ MA_ALINEAR_PAGINA
MA_SUMA_DE_CONTROL:     equ -(MA_NUMERO_MAGICO + MA_BANDERAS)


        section .text

        ;; Este salto permite que no explote todo cuando el núcleo se carga
        ;; sin seguir la Especificación de Multiarranque.
        jmp     cargar_nucleo

        ;; Encabezado de Multiarranque.
        align   4
        dd      MA_NUMERO_MAGICO
        dd      MA_BANDERAS
        dd      MA_SUMA_DE_CONTROL

        global  cargar_nucleo
cargar_nucleo:
        mov     esp, pila
        ;; Llamamos a la función inicial en C.
        push    ebx
        push    eax
        call    inicio


        section .bss
        resb    0x2000          ; Reservar 8 KiB de RAM para la pila
pila:
