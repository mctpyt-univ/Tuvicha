/***************************************************************************
 * Copyright © 2013 Mariano Street.                                        *
 * Este archivo es parte de Tuvicha.                                       *
 *                                                                         *
 * Tuvicha is free software: you can redistribute it and/or modify it      *
 * under the terms of the GNU General Public License as published by the   *
 * Free Software Foundation, either version 3 of the License, or (at your  *
 * option) any later version.                                              *
 *                                                                         *
 * Tuvicha is distributed in the hope that it will be useful, but WITHOUT  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *
 * for more details.                                                       *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with Tuvicha.  If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/


#include "idt.h"
#include "gdt.h"
#include "puerto.h"
#include "utilidades/asegurar.h"


/** Puerto del PIC maestro. */
#define PIC_MAESTRO    0x20

/** Puerto del PIC esclavo. */
#define PIC_ESCLAVO    0xA0

typedef struct {
    uint16 base0;
    uint16 selector;
    uint8  siempre_cero;
    uint8  banderas;
    uint16 base1;
} __attribute__((packed)) IDTDescriptor;

typedef struct {
    uint16 limite;
    uint32 base;
} __attribute__((packed)) IDTPuntero;

/**
 * IDT (“Interrupt Descriptor Table”): una tabla con las direcciones de los
 * manejadores de interrupción, en la que el índice de cada elemento indica
 * la interrupción a la que corresponde. La máxima longitud de la IDT es 256
 * entradas, por tanto todo valor representable en un byte es un índice
 * válido.
 */
static IDTDescriptor idt[0x100];

/**
 * Máscaras a establecer para cada PIC: cada bit corresponde a una IRQ según
 * su posición (el bit 0 corresponde a la IRQ 0, etc.), si está en 1 indica
 * que la interrupción está desactivada y si está en 0, que está activada.
 * Valor inicial: todas las IRQ desactivadas. A medida que se vayan agregando
 * manejadores de interrupciones, se irán habilitando IRQ.
 */
static uint16        mascara_pic = 0xFFFF;

/**
 * Programar los PIC: primero inicializarlos, mandando 4 ICW (“Interrupt
 * Control Words”) a cada uno, y después aplicarles máscaras. Esto es
 * necesario porque, en modo protegido, los rangos de índices de
 * interrupciones que usan por defecto los PIC son empleados por la CPU para
 * excepciones; y también porque puede pasar que no tengamos definidas
 * funciones para manejar las 16 interrupciones distintas generadas por los
 * PIC.
 */
static void inicializar_pic(void)
{
    /* Primera ICW: indica cuántas ICW se van a mandar y si ambos PIC deben
       trabajar en cascada. */
    puerto_escribir(PIC_MAESTRO,     0x11);
    puerto_escribir(PIC_ESCLAVO,     0x11);
    /* Segunda ICW: bases para los números de interrupciones a generar. */
    puerto_escribir(PIC_MAESTRO + 1, 0x20);
    puerto_escribir(PIC_ESCLAVO + 1, 0x28);
    /* Tercera ICW: números de IRQ para interconectar ambos PIC. */
    puerto_escribir(PIC_MAESTRO + 1,  0x4);
    puerto_escribir(PIC_ESCLAVO + 1,  0x2);
    /* Cuarta ICW: especifica el entorno donde se va a trabajar. */
    puerto_escribir(PIC_MAESTRO + 1,  0x1);
    puerto_escribir(PIC_ESCLAVO + 1,  0x1);
    /* Aplicamos máscaras en los PIC para que ignoren todas las IRQ menos
       aquellas a las que se hayan asignado manejadores. */
    puerto_escribir(PIC_MAESTRO + 1, mascara_pic & 0xFF);
    puerto_escribir(PIC_ESCLAVO + 1, mascara_pic >> 8);
}

void idt_instalar_manejador(const uint8  indice,
                            IDTManejador manejador)
{
    const uint32 dire = (uint32) manejador;

    idt[indice].base0        = dire & 0xFFFF;
    idt[indice].base1        = dire >> 16;
    idt[indice].selector     = GDT_SELECTOR_DE_CODIGO;
    idt[indice].siempre_cero = 0;
    idt[indice].banderas     = 0x8E;
}

void idt_instalar_manejador_irq(const uint8  irq,
                                IDTManejador manejador)
{
    asegurar(irq < 16);

    idt_instalar_manejador(0x20 + irq, manejador);
    /* Ponemos en 0 el bit `irq'-ésimo de la máscara para el PIC. */
    mascara_pic &= ~(1 << irq);
}

void idt_inicializar(void)
{
    volatile IDTPuntero idt_puntero;

    inicializar_pic();
    idt_puntero.limite = sizeof idt - 1;
    idt_puntero.base   = (uint32) idt;
    __asm__ __volatile__("lidt %0" : "=m" (idt_puntero));
    __asm__ __volatile__("sti");
}
