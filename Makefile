#====================
# Variables y rutas
#====================

OBJETIVO = tuvicha
AS       = nasm
ASFLAGS  = -f elf32
CFLAGS   = -std=c99 -Wall -Wextra -pedantic -Winline -Wredundant-decls \
           -Wuninitialized -Wmaybe-uninitialized -Wwrite-strings \
           -Wno-parentheses -O3 -ffreestanding
ifdef DEPURAR
    CFLAGS += -g -DDEPURACION
endif
LDFLAGS  = -T config.ld
# Advertencia: `cargador.o' debe estar entre los primeros 8 KiB del binario
# resultante, mientras más al principio mejor; por eso se recomienda dejarlo
# siempre primero en la lista que sigue.
OBJETOS  = objetos/cargador.o            \
           objetos/cga.o                 \
           objetos/comandos.o            \
           objetos/cpu_aux.o             \
           objetos/cpu.o                 \
           objetos/disco.o               \
           objetos/fat32.o               \
           objetos/gdt_aux.o             \
           objetos/gdt.o                 \
           objetos/idt.o                 \
           objetos/inicio.o              \
           objetos/interrupcion.o        \
           objetos/lector.o              \
           objetos/lineacomando.o        \
           objetos/ma.o                  \
           objetos/mbr.o                 \
           objetos/pantalla.o            \
           objetos/puerto.o              \
           objetos/teclado.o             \
           objetos/comandos_ayuda.o      \
           objetos/comandos_disco.o      \
           objetos/comandos_fat32.o      \
           objetos/comandos_limpiar.o    \
           objetos/comandos_luces.o      \
           objetos/comandos_particion.o  \
           objetos/comandos_reiniciar.o  \
           objetos/comandos_sistema.o    \
           objetos/comandos_teclado.o    \
           objetos/comandos_tuvicha.o    \
           objetos/utilidades_anillo.o   \
           objetos/utilidades_asegurar.o \
           objetos/utilidades_cadena.o   \
           objetos/utilidades_memoria.o  \
           objetos/utilidades_tiempo.o

# Nombre del disco virtual.
DISCO         = disco.img
# Tamaño del disco virtual, en MiB.
DISCO_TAMANIO = 50
# Cantidad de memoria RAM para las máquinas virtuales, en MiB.
MV_RAM        = 32
# Dispositivo de “loopback” usado por la regla ''discop''.
DISPOS_LOOP   = /dev/loop0

# Rutas donde buscar las dependencias.
vpath %.c fuente
vpath %.h fuente
vpath %.s fuente


#====================
# Reglas de comandos
#====================

.PHONY: todo all limpieza clean mv mvd

todo: $(OBJETIVO).bin
all: todo

limpieza:
	$(RM) -f $(OBJETIVO) $(OBJETOS)
clean: limpieza

# Ejecutar el binario final en una máquina virtual sin discos duros.
mv: $(OBJETIVO).bin
	qemu-system-i386 -m $(MV_RAM) -kernel $<

# Ejecutar el binario final en una máquina virtual con un disco duro virtual.
mvd: $(OBJETIVO).bin
	qemu-system-i386 -m $(MV_RAM) -kernel $< $(DISCO)

# Generar un disco duro virtual sin MBR y con un sistema de archivos FAT32.
disco:
	dd if=/dev/zero of=$(DISCO) bs=1M count=$(DISCO_TAMANIO)
	mkfs.vfat -F 32 $(DISCO)

# Generar un disco duro virtual con MBR, una partición primaria y un sistema
# de archivos FAT32 en dicha partición.
discop:
	lsmod | grep loop &>/dev/null
	dd if=/dev/zero of=$(DISCO) bs=1M count=$(DISCO_TAMANIO)
	losetup -o 512 $(DISPOS_LOOP) $(DISCO)
	echo ',,b' | sfdisk -q $(DISCO)
	mkfs.vfat -F 32 $(DISPOS_LOOP)
	losetup -d $(DISPOS_LOOP)


#====================
# Reglas de archivos
#====================

# Arrancador obsoleto.
#arranque.bin: arranque/arranque.s
#	$(AS) -f bin -i fuente/arranque/ -o $@ $^

$(OBJETIVO).bin: $(OBJETOS)
	$(LD) $(LDFLAGS) -o $@ $^

$(OBJETOS): | objetos

objetos:
	mkdir $@

objetos/%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

objetos/comandos_%.o: comandos/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

objetos/utilidades_%.o: utilidades/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

objetos/%.o: %.s
	$(AS) $(ASFLAGS) -o $@ $<

objetos/cga.o: cga.h              \
               puerto.h           \
               utilidades/comun.h \
               utilidades/memoria.h

objetos/cpu.o: cpu.h     \
               puerto.h  \
               teclado.h \
               utilidades/comun.h

objetos/comandos.o: comandos.h           \
                    lineacomando.h       \
                    comandos/ayuda.h     \
                    comandos/disco.h     \
                    comandos/fat32.h     \
                    comandos/limpiar.h   \
                    comandos/luces.h     \
                    comandos/particion.h \
                    comandos/reiniciar.h \
                    comandos/sistema.h   \
                    comandos/teclado.h

objetos/disco.o: disco.h            \
                 puerto.h           \
                 utilidades/comun.h \
                 utilidades/asegurar.h

objetos/fat32.o: fat32.h               \
                 disco.h               \
                 utilidades/asegurar.h \
                 utilidades/comun.h    \
                 utilidades/memoria.h  \
                 utilidades/tiempo.h

objetos/gdt.o: gdt.h \
               utilidades/comun.h

objetos/idt.o: idt.h                 \
               gdt.h                 \
               puerto.h              \
               utilidades/asegurar.h \
               utilidades/comun.h

objetos/inicio.o: cpu.h          \
                  disco.h        \
                  gdt.h          \
                  idt.h          \
                  interrupcion.h \
                  lector.h       \
                  lineacomando.h \
                  ma.h           \
                  pantalla.h     \
                  utilidades/comun.h

objetos/lector.o: lector.h            \
                  lineacomando.h      \
                  pantalla.h          \
                  utilidades/anillo.h \
                  utilidades/cadena.h \
                  utilidades/comun.h

objetos/lineacomando.o: lineacomando.h        \
                        pantalla.h            \
                        utilidades/asegurar.h \
                        utilidades/cadena.h   \
                        utilidades/comun.h

objetos/ma.o: ma.h                  \
              utilidades/asegurar.h \
              utilidades/comun.h

objetos/mbr.o: mbr.h                 \
               disco.h               \
               utilidades/asegurar.h \
               utilidades/comun.h

objetos/pantalla.o: pantalla.h            \
                    cga.h                 \
                    utilidades/asegurar.h \
                    utilidades/comun.h

objetos/puerto.o: puerto.h \
                  utilidades/asegurar.h

objetos/teclado.o: teclado.h             \
                   lector.h              \
                   puerto.h              \
                   utilidades/asegurar.h \
                   utilidades/comun.h    \
                   utilidades/cadena.h

objetos/comandos_ayuda.o: comandos/ayuda.h \
                          lineacomando.h   \
                          pantalla.h       \
                          utilidades/comun.h

objetos/comandos_disco.o: comandos/disco.h \
                          disco.h          \
                          lineacomando.h   \
                          mbr.h            \
                          pantalla.h       \
                          utilidades/cadena.h

objetos/comandos_fat32.o: comandos/fat32.h    \
                          disco.h             \
                          fat32.h             \
                          lineacomando.h      \
                          mbr.h               \
                          pantalla.h          \
                          utilidades/cadena.h \
                          utilidades/tiempo.h

objetos/comandos_limpiar.o: comandos/limpiar.h \
                            pantalla.h

objetos/comandos_luces.o: comandos/luces.h \
                          pantalla.h       \
                          teclado.h

objetos/comandos_particion.o: comandos/particion.h \
                              pantalla.h

objetos/comandos_reiniciar.o: comandos/reiniciar.h \
                              cpu.h                \
                              pantalla.h

objetos/comandos_sistema.o: comandos/sistema.h \
                            cpu.h              \
                            ma.h               \
                            pantalla.h

objetos/comandos_teclado.o: comandos/teclado.h \
                            lineacomando.h     \
                            pantalla.h         \
                            teclado.h

objetos/comandos_tuvicha.o: comandos/tuvicha.h \
                            pantalla.h

objetos/utilidades_anillo.o: utilidades/anillo.h   \
                             utilidades/asegurar.h \
                             utilidades/cadena.h   \
                             utilidades/comun.h

objetos/utilidades_asegurar.o: utilidades/asegurar.h \
                               cpu.h                 \
                               pantalla.h

objetos/utilidades_cadena.o: utilidades/cadena.h   \
                             utilidades/asegurar.h \
                             utilidades/comun.h

objetos/utilidades_memoria.o: utilidades/memoria.h \
                              utilidades/asegurar.h

objetos/utilidades_tiempo.o: utilidades/tiempo.h \
                             utilidades/cadena.h \
                             utilidades/comun.h  \
                             pantalla.h
